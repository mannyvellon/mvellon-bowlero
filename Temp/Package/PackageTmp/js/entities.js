﻿if (typeof (entities) == "undefined")
{ entities = { __namespace: true }; }
entities = {
    event: function (id, resource, end, start, userid, text, siteid, backColor, oppoid, repname, partyname, guestcount, onholddate, isretailhold,iseventhold ,totalrevenue, createdbyname, isbufferactivity, isleague,oppoStart,oppoEnd)
    {
        this.id = id,
         this.resource = resource,
        this.end = end,
        this.start = start,
        this.userid = userid,
        this.text= text,
        this.siteid = siteid,
        this.backColor=backColor,
        this.oppoid = oppoid,
        this.repname = repname,
        this.partyname = partyname,
        this.guestcount = guestcount,
        this.onholddate = onholddate,
        this.isretailhold = isretailhold,
        this.iseventhold = iseventhold,
        this.totalrevenue = totalrevenue,
        this.createdbyname = createdbyname,
        this.isbufferactivity = isbufferactivity,
        this.isleague = isleague,
        this.oppoStart = oppoStart,
        this.oppoEnd = oppoEnd,
        this.isDeleted=false
    },
    resource: function (name,id,expanded,children,type) {
        this.name = name,
        this.id = id,
        this.expanded = expanded,
        this.children = children,
        this.type=type
    },
    site: function (Id,Name,order) {
        this.Id = Id,
        this.Name = Name,
        this.order = order
    }
}