/// <reference path="restapi.js" />
/// <reference path="entities.js" />

if (typeof (common) == "undefined")
{ common = { __namespace: true }; }
common = {
    _getEvents: function (siteid, sdate, edate, loadEvents) {
        var cevents = [];
        //TODO CRM
        // var userid = GetGlobalContext().getUserId();
        //local
        var userid = "C81F2774-5EF4-E511-80C2-005056AA8521";
        // alert(userid);
        restapi._get('activities/' + siteid + '/' + sdate + '/' + edate + '/' + userid, function (data) {
            if (data.length > 0) {
                //var items = data.map(JSON.parse)
                $.each(data, function (key, val) {
                    var ev = $.extend(new entities.event, val);
                    cevents.push(ev);
                })
            }
            loadEvents(cevents);
        }
        );
        //return cevents;
    },
    _getResources: function (siteid, loadResources) {
        var cresources = [];
        restapi._get('resources/' + siteid, function (data) {
            if (data.length > 0) {
                //var items = data.map(JSON.parse)
                $.each(data, function (key, val) {
                    var ev = $.extend(new entities.resource, val);
                    cresources.push(ev);
                })
            }
            loadResources(cresources);
            //cresources = data;
        })
        //return cresources;
    },
    _getSites: function (loadSites) {
        var csites = [];
        restapi._get('sites', function (data) {

            if (data.length > 0) {
                //var items = data.map(JSON.parse)
                $.each(data, function (key, val) {
                    var ev = $.extend(new entities.site, val);
                    csites.push(ev);
                })
            };
            loadSites(csites);
        })
        //  return csites;
    },
    _createActivity: function (data, successfn) {
        restapi._post('activity/create', JSON.stringify(data), successfn);
    },
    _updateActivity: function (data, successfn) {
        restapi._post('activities/update', JSON.stringify(data), successfn);
    },
    _updateEvent: function (data, successfn) {
        restapi._post('event/update', JSON.stringify(data), successfn);
    },
    _updateActivities: function (data, successfn) {
        restapi._post('activities/update', JSON.stringify(data), successfn);
    },
    _cancelActivity: function (data, successfn) {
        restapi._post('activities/cancel', JSON.stringify(data), successfn);
    },
    _getOppoUrl: function (eventId, localEvents) {
        //<http://mycrm/myOrg/main.aspx?etc=4&id=%7b899D4FCF-F4D3-E011-9D26-00155DBA3819%7d&pagetype=entityrecord>
        var oppId = "";
        $.each(localEvents, function (key, val) {
            if (val.id == eventId) {
                oppId = val.oppoid;
                return;
            }
        });
        if (oppId == null || oppId == undefined || oppId =="") {
            alert("No Event exist!");
            return null;
        }
        var orgUrl = GetGlobalContext().getClientUrl();
        return orgUrl + "/main.aspx?etc=3&id=%7b" + oppId + "%7d&pagetype=entityrecord";
    },
    _isRetailHold: function (eventId, localEvents) {
        var isRetail = false;
        $.each(localEvents, function (key, val) {
            if (val.id == eventId && val.isretailhold) {
                isRetail = true;
                return;
            }
        });
        return isRetail;
    },
    _isEventHold: function (eventId, localEvents) {
        var isEvent = false;
        $.each(localEvents, function (key, val) {
            if (val.id == eventId && val.iseventhold) {
                isEvent = true;
                return;
            }
        });
        return isEvent;
    },
    _isLeague: function (eventId, localEvents) {
        var isLeague = false;
        $.each(localEvents, function (key, val) {
            if (val.id == eventId && val.isleague) {
                isLeague = true;
                return;
            }
        });
        return isLeague;
    },
    _getRelated: function (eventId, localEvents) {
        var oppId = "";
        var relatedEvents = [];
        $.each(localEvents, function (key, val) {
            if (val.id == eventId) {
                oppId = val.oppoid;
                return;
            }
        });
        $.each(localEvents, function (key, val) {
            if (val.oppoid == oppId && val.oppoid != undefined && val.oppoid != "" && val.oppoid != null) {
                relatedEvents.push(val);
            }
        });
        return relatedEvents;
    }
}
//converting dropdown list to auto complete combobox
if (typeof (autocombobox) == "undefined")
{ autocombobox = { __namespace: true }; }
autocombobox = {
    _convertToCombobox : function () {
    $.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
             .addClass("custom-combobox")
              .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"),
              value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
              .appendTo(this.wrapper)
              .val(value)
              .attr("title", "")
              .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
              .autocomplete({
                  delay: 0,
                  minLength: 0,
                  source: $.proxy(this, "_source")
              })
              .tooltip({
                  classes: {
                      "ui-tooltip": "ui-state-highlight"
                  }
              });

            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function () {
            var input = this.input,
              wasOpen = false;

            $("<a>")
              .attr("tabIndex", -1)
             // .attr("title", "Show All Items")
              .tooltip()
              .appendTo(this.wrapper)
              .button({
                  icons: {
                      primary: "ui-icon-triangle-1-s"
                  },
                  text: false
              })
              .removeClass("ui-corner-all")
              .addClass("custom-combobox-toggle ui-corner-right")
              .on("mousedown", function () {
                  wasOpen = input.autocomplete("widget").is(":visible");
              })
              .on("click", function () {
                  input.trigger("focus");

                  // Close if already visible
                  if (wasOpen) {
                      return;
                  }

                  // Pass empty string as value to search for, displaying all results
                  input.autocomplete("search", "");
              });
        },

        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },

        _removeIfInvalid: function (event, ui) {

            // Selected an item, nothing to do
            if (ui.item) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
              valueLowerCase = value.toLowerCase(),
              valid = false;
            this.element.children("option").each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if (valid) {
                return;
            }

            // Remove invalid value
            this.input
              .val("")
              .attr("title", value + " didn't match any item")
              .tooltip("open");
            this.element.val("");
            this._delay(function () {
                this.input.tooltip("close").attr("title", "");
            }, 2500);
            this.input.autocomplete("instance").term = "";
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

    $("#sites").combobox();
    //$("#toggle").on("click", function () {
    //    $("#sites").toggle();
    //});
}
}
if (typeof (entities) == "undefined")
{ entities = { __namespace: true }; }
entities = {
    event: function (id, resource, end, start, userid, text, siteid, backColor, oppoid, repname, partyname, guestcount, onholddate, isretailhold,iseventhold ,totalrevenue, createdbyname, isbufferactivity, isleague,oppoStart,oppoEnd)
    {
        this.id = id,
         this.resource = resource,
        this.end = end,
        this.start = start,
        this.userid = userid,
        this.text= text,
        this.siteid = siteid,
        this.backColor=backColor,
        this.oppoid = oppoid,
        this.repname = repname,
        this.partyname = partyname,
        this.guestcount = guestcount,
        this.onholddate = onholddate,
        this.isretailhold = isretailhold,
        this.iseventhold = iseventhold,
        this.totalrevenue = totalrevenue,
        this.createdbyname = createdbyname,
        this.isbufferactivity = isbufferactivity,
        this.isleague = isleague,
        this.oppoStart = oppoStart,
        this.oppoEnd = oppoEnd,
        this.isDeleted=false
    },
    resource: function (name,id,expanded,children,type) {
        this.name = name,
        this.id = id,
        this.expanded = expanded,
        this.children = children,
        this.type=type
    },
    site: function (Id,Name,order) {
        this.Id = Id,
        this.Name = Name,
        this.order = order
    }
}
if (typeof (helper) == "undefined")
{ helper = { __namespace: true }; }
helper = {
    _getUrlVars: function () {
        var vars = [], hash;
        var dUrl = decodeURIComponent(window.location.href);
        var hashes = dUrl.slice(dUrl.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    _getUrlVar: function (name) {
        return $.getUrlVars()[name];
    },
    //function to add days to a given date. 
    _addDays : function(startDate, numberOfDays) {
    var returnDate = new Date(
                        startDate.getFullYear(),
                        startDate.getMonth(),
                        startDate.getDate() + numberOfDays
                        );
    return returnDate;
    // return year + '-' + month + '-' + (date + numberOfDays);
    //return returnDate;
}
       
}


function GetGlobalContext() {
    console.log("Call to GCC");
    return {
        getUserId: function () { return 'C81F2774-5EF4-E511-80C2-005056AA8521'; },
        getUserName: function () { return 'Robert Roberts'; },
        regClientUrl: function () { return 'https://locahost:44350'}

    }
}
//code for loading all the sites
function loadSites() {
    var $select = $('#sites');
    //clear sites select
    $select.html('');
    //get json data from crm
    common._getSites(function (sites) {
        $select.append('<option value="-1"></option>');
        $.each(sites, function (key, val) {
            $select.append('<option  value="' + val.Id + '">' + val.Name + '</option>');
        });
        //get the siteid from URL
        var siteId = helper._getUrlVars()["site"];
        if (siteId != undefined && siteId != null) {
            $("#sites").val(siteId);
            
        }
        autocombobox._convertToCombobox();
    }
    );
};
//loading all the events
function loadEvents(siteId, start, end) {
    var isEventsFound = false;
    dp.events.list = [];
    dp.update();
    common._getEvents(siteId, start, end, function (crmEvents) {
        localEvents = crmEvents;
        isEventsFound = true;
        $.each(crmEvents, function (key, val) {
            // alert(val.start);
            var e = new DayPilot.Event({
                start: new DayPilot.Date(val.start),
                end: new DayPilot.Date(val.end),
                id: val.id,
                resource: val.resource,
                text: val.text,
                backColor: val.backColor,
                repname: val.repname,
                partyname: val.partyname,
                guestcount: val.guestcount,
                onholddate: val.onholddate,
                totalrevenue: val.totalrevenue,
                oppoid: val.oppoid,
                resizeDisabled: true,
                isretailhold: val.isretailhold,
                iseventhold: val.iseventhold,
                createdbyname: val.createdbyname
            });
            dp.events.add(e);
        });
        //dp.message("Events Loaded!!");
        if (isEventsFound) {
            dp.message("Events Loaded!!", 1000, "#ffff", "#dc143c");
        }
    });

}
//loading all the resources
function loadResources(siteId) {
    barriers = [];
    timeSensitiveResources = [];
    common._getResources(siteId, function (resources) {
        localResources = [];
        resourceColors=[];
        getListResources(resources);
        var i = 0; j = 0;
        $.each(resources, function (key, val) {
            if (val.type == "Floor" || val.type == "PhysicalBarrier" || val.type == "NotSpecified") {
                barriers.push(val.id);
            }
        });
        resourceColors = getColorCodeResources(resources, i, resourceColors);
        timeSensitiveResources = getTimeSensitiveResources(resources, j, timeSensitiveResources);
        dp.resources = resources;
        dp.update();
    });
}

function getTimeSensitiveResources(resources, j, timeSenResource) {
    $.each(resources, function (key, val) {
        if (val.type == "BowlingLane" || val.type == "Room")//BowlingLane and rooms are time sensitive resources, any changes to time create contract revision
        {
            timeSenResource.push(val.id);
        }
        j++;
        if (val.children != null && val.children != undefined && val.children != "undefined" && val.children.length > 0) {
            getTimeSensitiveResources(val.children, j, timeSenResource);
            j = j + val.children.length;
        }
    });
    return timeSenResource;
}
function getColorCodeResources(resources, i, resourceColors)
{
    $.each(resources, function (key, val) {
        if (i % 2 == 0)//even rows use light gray color #deeff5
        {
            resourceColors.push(val.id);
        }
        i++;
        if (val.children != null && val.children != undefined && val.children != "undefined" && val.children.length > 0) {
            getColorCodeResources(val.children, i, resourceColors);
            i = i + val.children.length;
        }
    });
    return resourceColors;
}
//cancelling activity
function cancelActivity(event, e) {
    common._cancelActivity(event, function () {
        dp.events.remove(e);
        dp.message("Event Cancelled Successfully!!", 5000, "#fff", "#dc143c");
    });
}
//update activity
function updateActivity(event, e) {
    common._updateActivity(event, function () {
        ////update local list of events
        $.each(localEvents, function (key, val) {
            if (val.resource == e.resource()
                && val.start == e.start().value &&
                val.end == e.end().value) {
                val.resource = event.resource;
                val.start = event.start;
                val.end = event.end;
                return;
            }
        });
        e.resource(event.resource);
        e.start(event.start);
        e.end(event.end);
        dp.events.update(e);

        dp.message("Event Updated!!", 5000, "#fff", "#dc143c");
    });

}
//update list of activities in CRM
function updateActivities(events, isTimeChanged) {
    if (events[0].oppoid == "" || events[0].oppoid == undefined || events[0].oppoid == null)
    {
        //retail event is being updated
        dp.message("Updating event(s)!", 5000, "#fff", "#dc143c");
    }
    else if (isTimeChanged)
    {
        dp.message("Updating event(s) and revising contract!", 5000, "#fff", "#dc143c");
    }
    else
    {
        dp.message("Updating event(s)!", 5000, "#fff", "#dc143c");
    }
    common._updateActivities(events, function () {
        //retail event is being updated
        if (events[0].oppoid == "" || events[0].oppoid == undefined || events[0].oppoid == null) {
            //refresh data from CRM, executing siteonclick to refresh data
            //loadSiteData();
            refreshEvents();
            dp.message("Event(s) Updated!", 5000, "#fff", "#dc143c");
        }
        //if time is changed, update opportunity & contract to reflect changes
        else if (isTimeChanged) {
            //check if multiple activities are processed/
            //call method to change event start & end and also revise contract
            common._updateEvent(events[0], function () {
                dp.multiselect.clear();
                //refresh data from CRM, executing siteonclick to refresh data
                refreshEvents();
                dp.message("Event(s) Updated & Contract Revised!!", 5000, "#fff", "#dc143c");
            });
        }
        else
        {
            dp.multiselect.clear();
            //refresh data from CRM, executing siteonclick to refresh data
            refreshEvents();
            dp.message("Event Updated!!", 5000, "#fff", "#dc143c");
        }
        
    });
}
//create activity
function createActivity(event) {
    if (event.isretailhold) {
        dp.message("Creating Retail Hold!!", 5000, "#fff", "#dc143c");
    }
    else {
        dp.message("Creating Event Hold!!", 5000, "#fff", "#dc143c");
    }
    common._createActivity(event, function (val) {
        refreshEvents();
        dp.message("Event Created!!", 5000, "#fff", "#dc143c");
    });
}
//get list of resources
function getListResources(resources) {
    $.each(resources, function (key, val) {
        var rs = new entities.resource(val.name, val.id, val.expanded, val.children, val.type);
        localResources.push(rs);
        if (val.expanded) {
            getListResources(val.children);
        }
    });
}
//get resource by id
function resourceById(rid) {
    var rs = {};
    $.each(localResources, function (key, val) {
        if (val.id == rid) {
            rs = val;
            return;
        }
    });
    return rs;
}
//check if resource is child
function isChild(children,resource)
{
    var isChild = false;
    $.each(children, function (key, val) {
        if (val.id == resource) {
            isChild = true;
            return;
        }
    });
    return isChild;
}
//check if event is buffer activity
function isBuffer(eventId, allEvents) {
    var isBuffer = false;
    $.each(allEvents, function (key, val) {
        if (val.id == eventId && val.isbufferactivity) {
            isBuffer = true;
            return isBuffer;
        }
    });
    return isBuffer;
}
//get resource by id
function eventById(eid) {
    var ev = {};
    $.each(localEvents, function (key, val) {
        if (val.id == eid) {
            ev = val;
            return;
        }
    });
    return ev;
}
//code for getting activites based on start & end date and entertainment center selected.
function loadSiteData() {
    var siteId = getCurrentSiteId();
    // alert(siteId);
    if (siteId != -1 && siteId != undefined && siteId != null) {
        loadResources(siteId);
        refreshEvents(siteId);
        //move scroll to time passed
        var st = helper._getUrlVars()["st"];
        if (st != undefined && st != "undefined" && st != "") {
            dp.scrollTo(dp.startDate.addHours(st));
        } else {
            dp.scrollTo(dp.startDate.addHours(12));
        }
        dp.update();    
    }

}
//getting events from crm
function refreshEvents() {
    var siteId = getCurrentSiteId();
    if (siteId == undefined || siteId == '')
    {
        return;
    }
    //var siteId = $('#sites').find(":selected").val();
    var startDate = new Date(nav.selectionDay.addHours(12)); // new Date(nav.selectionDay.substring(0,nav.selectionDay.indexOf('T')));
    var enddate = helper._addDays(startDate, 3);
    var start = (startDate.getMonth() + 1) + '-' + startDate.getDate() + '-' + startDate.getFullYear();
    var end = (enddate.getMonth() + 1) + '-' + enddate.getDate() + '-' + enddate.getFullYear();
    loadEvents(siteId, start, end);
}
function getCurrentSiteId() {
    var siteId = $('#sites').find(":selected").val();
    if(siteId == '' || siteId == undefined)
    {
        siteId = helper._getUrlVars()["site"];
    }
    return siteId;
}
//setting dates from query string parameters
function setDates() {
    var startDate = helper._getUrlVars()["sd"];
    var siteId = helper._getUrlVars()["site"];
    var config = helper._getUrlVars()["config"];
    if (config == "true")
    {
        dp.height = 220;
        dp.update();
    }
    if (siteId != undefined && siteId != null) {
      //  $("#sites").val(siteId);
        var dpDate = new DayPilot.Date(startDate);
        if (dpDate.getDatePart().value == nav.selectionDay.getDatePart().value) {
            //as site is not selected initially we will load the data after setting data
            loadSiteData();
        }
        else {           
            nav.select(dpDate);
        }
        //else {
          //  nav.select(dpDate);
        //}
    }
};
if (typeof (restapi) == "undefined")
{ restapi = { __namespace: true }; }
restapi = {
    _getCrmServerUrl: function () {
        return "https://localhost/44353";
    },
    _errorfn: function (request, message, error)
    {
        if (request.responseText!= undefined && request.responseText!="") {
            // only parse the response if you know it is JSON
            var error = $.parseJSON(request.responseText);
            alert(error.Message);
        }
        else if (request.responseText == "" || request.responseText == undefined) {
            //this would be not found exception from server as per request from customer not displaying any error.
        }
        else {
            alert('An error occurred while processing your request.');
        }

    },
    _restServiceURL: function () {
        ///<summary>
        /// service url
        ///</summary>
        ///<returns>rest service url</returns>
        //local variable from html file

        //TODO
       //return "http://localhost:61985/";
       //return "http://dev-gemsns01.na.amfbowl.net/";
            return "https://localhost:44353/";

    },
    _get: function (url, successfn) {
          $.ajax({
              dataType: "json",
              async: true,
            url: this._restServiceURL() + url,//'activities/GetLActivities',
            success: successfn,
            error: this._errorfn
          });
    },

    _post: function (url, data, successfn) {
        $.ajax({
            type: "POST",
            dataType: "json",
            async: true,
            contentType : "application/json",
            url: this._restServiceURL() + url,//'activities/lanegrid/updateserviceactivity',
            data: data,
            success: successfn,
            error: this._errorfn
        });
    }
}

if (typeof (scheduler) == "undefined")
{ scheduler = { __namespace: true }; }
scheduler = {
    _init: function () {

        nav = new DayPilot.Navigator("nav");
        nav.theme = "navigator_blue";
        nav.showMonths = 2;
        nav.selectMode = "day";
        nav.onTimeRangeSelected = function (args) {
            dp.startDate = args.start;
            //alert(args.days);
            dp.days = args.days + 2; //displaying current day + 2 days
            loadSiteData();
            dp.update();
        };
        nav.init();
        // view
        //  dp.startDate = new DayPilot.Date("2013-03-24");  // or just dp.startDate = "2013-03-25";
        // view
        dp = new DayPilot.Scheduler("dp");
        dp.heightSpec = "Max";
        dp.height = 600;
        // behavior and appearance
        //dp.cssClassPrefix = "scheduler_transparent";
        dp.theme = "scheduler_blue";


        //     dp.resources = [];
        dp.startDate = nav.selectionStart;  // or just dp.startDate = "2013-03-25";
        dp.cellGroupBy = "Month";
        dp.cellWidth = 25;
        dp.days = 3;
        dp.scale = "CellDuration";
        dp.cellDuration = 15;
        // dp.days = 7;
        //dp.cellDuration = 1440; // one day

        dp.timeHeaders = [
             { groupBy: "Day" },
            { groupBy: "Hour" },
            { groupBy: "Cell" }
        ];
        //business hours
        dp.businessBeginsHour = 0;
        dp.businessEndsHour = 24;
        dp.showNonBusiness = true;
        dp.eventDoubleClickHandling = true;
        //context menu for Cancel
        dp.contextMenu = new DayPilot.Menu({
            items: [
                {
                    text: "Cancel",

                    onclick: function () {

                        var e = this.source;
                        var ev = new entities.event(e.id());
                        if (common._isRetailHold(e.id(), localEvents) || common._isEventHold(e.id(), localEvents)) {

                            cancelActivity(ev, e);
                            $.each(localEvents, function (key, val) {
                                if (val.id == e.id()) {
                                    val.isDeleted = true;
                                    return;
                                }
                            });
                        }
                        else {
                            alert('Only retail activities can be cancelled!')
                        }
                    }
                },
            ],
            cssClassPrefix: "menu_default"
        });
        //to enable tree view for resources
        dp.treeEnabled = true;
        //dp.eventClickHandling = "Select";
        dp.allowMultiMove = true;
        dp.multiMoveVerticalMode = "All";
        //dps.allowEventOverlap = false;
        // bubble, sync loading
        // see also DayPilot.Event.data.staticBubbleHTML property
        dp.bubble = new DayPilot.Bubble();
        dp.eventHoverHandling = "Bubble";

        dp.init();

        dp.onBeforeEventRender = function (args) {
            var html = "";
            if (args.e.isretailhold || args.e.iseventhold) {
                html = "<div>Created By: " + args.e.createdbyname + "</div>"
                + "<div>On Hold Date: " + args.e.onholddate + "</div>";
            }
            else if (args.e.onholddate != "") {
                html = "<div>Event Name:<b>" + args.e.text + "</b></div>"
                    + "<div>Party Name: " + args.e.partyname + "</div>"
                  + "<div>Rep Name: " + args.e.repname + "</div>"
                  + "<div>Guest Count: " + args.e.guestcount + "</div>"
               + "<div>On Hold Date: " + args.e.onholddate + "</div>"
               + "<div>Total Revenue: " + args.e.totalrevenue + "</div>";
            }
            else {
                html = "<div>Event Name:<b>" + args.e.text + "</b></div>"
                                     + "<div>Party Name: " + args.e.partyname + "</div>"
                                   + "<div>Rep Name: " + args.e.repname + "</div>"
                                   + "<div>Guest Count: " + args.e.guestcount + "</div>"
                                + "<div>Total Revenue: " + args.e.totalrevenue + "</div>";
            }
            args.e.bubbleHtml = html;
        };
        //disable resize of event
        dp.onEventResized = function (args) {
            // args.preventDefault();
            return;
        };
        $(document).keyup(function (e) {
            // enter
            if (e.keyCode === 27) dp.multiselect.clear();   // esc
        });
        // event creating
        dp.onTimeRangeSelected = function (args) {
            //checking it the ctrl is pressed and skip retail if that is pressed
            if (args.ctrl) {
                args.preventDefault();
                return;
            }
            var ans = confirm('Do you want to create event hold?');
            var name = ""; var isEventHold = false; isRetailHold = false;
            dp.multiselect.clear();
            if (ans) {
                name = prompt("Event Hold Description:", "Event Hold");
                isEventHold = true;
            }
            else {
                name = prompt("Retail Hold Description:", "Retail Hold");
                isRetailHold = true;
            }

            var isBooked = false;
            //check if resource is booked with other events
            $.each(localEvents, function (key, val) {
                if (val.resource == args.resource  //check if both event are at same resource
                   && !val.isDeleted && ((val.start <= args.start && val.end > args.start) //existing event start date is less than current and end is greater than start
                  || (val.start > args.start && val.end <= args.end) //existing event start date is equal to current and end is greater than start                       
                    || (val.start < args.end && val.end > args.end) //existing event start date is equal to current and end is greater than start                          
                    )) {
                    isBooked = true;
                    return;
                }
            });
            if (isBooked) {
                alert('Resource(s) is already booked!');
                args.preventDefault();
                dp.clearSelection();
                return;
            }
            //check if 
            dp.clearSelection();
            if (!name) return;
            var e = new DayPilot.Event({
                start: args.start,
                end: args.end,
                id: DayPilot.guid(),
                resource: args.resource,
                text: name,
                backColor: "Grey",
                repname: "",
                partyname: "",
                guestcount: "",
                onholddate: "",
                totalrevenue: "",
                oppoid: "",
                isretailhold: true,
                resizeDisabled: true
            });

            //TODO - CRM
            var userid = GetGlobalContext().getUserId();
            var currentUserName = GetGlobalContext().getUserName();
            //local
            //TODO
            // var userid = "C81F2774-5EF4-E511-80C2-005056AA8521";
            //var currentUserName = "Amol";
            var siteId = $('#sites').find(":selected").val();
            //var currentUserName = "amol";
            var ev = new entities.event(e.id(), args.resource, args.end.value, args.start.value, userid, name, siteId, "Grey", "", "", "", "", "", isRetailHold, isEventHold, "", currentUserName);
            createActivity(ev, e);
        };
        // event moving
        //dp.eventMoveHandling = "JavaScript";
        dp.onEventMove = function (args) {

            var e = args.e;
            if ($.inArray(args.newResource, barriers) > -1) {
                alert('Cannot book resource!');
                args.preventDefault();
                return;
            }
            //check if it is league activity
            if (common._isLeague(e.id(), localEvents)) {
                alert('Cannot move League activities!');
                args.preventDefault();
                return;
            }
            //get resource object type 
            if (resourceById(e.resource()).type != resourceById(args.newResource).type) {
                alert('Cannot change to different type of resource!');
                args.preventDefault();
                return;
            }
        };
        dp.onEventMoved = function (args) {

            //update events send to crm
            var evs = [];
            var isTimeChanged = false;
            var isBooked = false; var isOutOppoBound = false;
            var isTimeSensitive = false;
            var isDiffResource = false;
            //validate if all the resources are avalible
            args.multimove.map(function (item) {
                var e = item.event;
                var currentEvent = eventById(e.id());
                //check if resource is booked with other events
                $.each(localEvents, function (key, val) {
                    if (val.id == e.id() && resourceById(e.resource()).type != resourceById(val.resource).type) {
                        console.log("local event " + val.id + " current event " + e.id() + " resource type local " + resourceById(val.resource).type
                            + " resource type current " + resourceById(e.resource()).type);
                        isDiffResource = true;
                        return;
                    }
                    if (val.resource == e.resource()  //check if both event are at same resource
                      && !val.isDeleted && val.id != e.id()          //check if both events are not same
                        && (args.multimove.length == 1 || currentEvent.oppoid != val.oppoid) //check if it is related resource movement and checking overbooking among children
                        && ((e.start().value < val.start && e.end().value > val.start && !val.isbufferactivity) //existing event start date is less than current and end is greater than start
                      || (e.start().value >= val.start && e.start().value < val.end && !val.isbufferactivity)  //existing event start date is equal to current and end is greater than start                       
                        )) {
                        isBooked = true;
                        return;
                    }
                });

                // args.e.resource(args.newResource);
                //TODO CRM
                var userid = GetGlobalContext().getUserId();
                var oppId = currentEvent.oppoid;
                if (currentEvent.start != e.start() || currentEvent.end != e.end()) {
                    isTimeChanged = true;
                }
                //check if the time has changes and the resource is one of the time sensitive resource
                if (isTimeChanged && !isBuffer(e.id(), localEvents) && $.inArray(e.resource(), timeSensitiveResources) > -1) {
                    isTimeSensitive = true;
                }
                //check event movement is outside event start & end bound
                var oppoStart = new DayPilot.Date(currentEvent.oppoStart);
                var oppoEnd = new DayPilot.Date(currentEvent.oppoEnd);
                if ((e.start() < oppoStart || e.end() > oppoEnd) && !isBuffer(e.id(), localEvents)) {
                    isOutOppoBound = true;
                }

                //local
                // var userid = "C81F2774-5EF4-E511-80C2-005056AA8521";

                if (args.multimove != undefined && args.multimove != null && args.multimove.length > 1) {
                    ev = new entities.event(e.id(), e.resource(), e.end(), e.start(), userid, "", "", "", oppId);
                }
                else {
                    //for single move, don't allow events to change the timings
                    ev = new entities.event(e.id(), args.newResource, args.newEnd.value, args.newStart.value, userid, "", "", "", oppId);
                }
                evs.push(ev);
            });
            if (isDiffResource) {
                alert('Cannot change to different type of resource!');
                args.preventDefault();
                refreshEvents();
                return;
            }
            if (isBooked) {
                alert('Resource(s) is already booked!');
                args.preventDefault();
                refreshEvents();
                return;
            }
            if (isTimeSensitive) {
                var ans = confirm('Changing time would revise the contract, do you still want to continue?');
                if (ans) {
                    updateActivities(evs, isTimeSensitive);
                }
                else {
                    args.preventDefault();
                    refreshEvents();
                    return;
                }
            }
            else if (isOutOppoBound) {
                var ans = confirm('Resource(s) are moved outside contract start/end time and it would revise the contract, do you still want to continue?');
                if (ans) {
                    updateActivities(evs, isTimeSensitive);
                }
                else {
                    args.preventDefault();
                    refreshEvents();
                    return;
                }
            }
            else {
                updateActivities(evs);
            }
        };

        dp.onBeforeCellRender = function (args) {
            if ($.inArray(args.cell.resource, barriers) > -1) {
                args.cell.backColor = "black";
            }
            else {
                //providing alternate color for lane grid
                if ($.inArray(args.cell.resource, resourceColors) > -1) {
                    args.cell.backColor = "#e7f5fe";
                }
            }

        };

        dp.onEventDoubleClick = function (args) {
            //  alert(restapi._restServiceURL());
            // alert("clicked: " + args.e.id());
            var url = common._getOppoUrl(args.e.id(), localEvents);
            if (url != undefined) {
                window.open(url);
            }
        };

        dp.onEventClick = function (args) {
            //check if selected activity is League event
            //don't allow user to select it
            if (common._isLeague(args.e.id(), localEvents)) {
                //
                alert('Cannot select League activities.');
                args.preventDefault();
                return;
            }
            if (args.ctrl) {
                dp.multiselect.add(args.e);   // add to selection 
                args.preventDefault();        // cancel the default action
            }
            else {
                var ans = confirm('Do you want to select related resources?');
                dp.multiselect.clear();
                if (ans) {
                    //dp.multiselect.clear();  
                    var relatedEvents = common._getRelated(args.e.id(), localEvents);
                    var resId = args.e.resource();
                    var selectedResource = resourceById(resId);
                    $.each(relatedEvents, function (val, e) {
                        var childResource = resourceById(e.resource);
                        if ((selectedResource.type == childResource.type) ||
                            (selectedResource.children != null && selectedResource.children != undefined && selectedResource.children.length > 0 && isChild(selectedResource.children, childResource.id))) {
                            //need to add logic to select related events
                            var e1 = new DayPilot.Event({
                                id: e.id,
                                start: e.start,
                                end: e.end,
                                resource: e.resource
                            });
                            dp.multiselect.add(e1);
                        }
                        //write to code to update CRM
                    });
                }
                else {
                    dp.multiselect.add(args.e);
                }
            };
        }

    }
}