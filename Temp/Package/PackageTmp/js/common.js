﻿/// <reference path="restapi.js" />
/// <reference path="entities.js" />

if (typeof (common) == "undefined")
{ common = { __namespace: true }; }
common = {
    _getEvents: function (siteid, sdate, edate, loadEvents) {
        var cevents = [];
        //TODO CRM
        // var userid = GetGlobalContext().getUserId();
        //local
        var userid = "C81F2774-5EF4-E511-80C2-005056AA8521";
        // alert(userid);
        restapi._get('activities/' + siteid + '/' + sdate + '/' + edate + '/' + userid, function (data) {
            if (data.length > 0) {
                //var items = data.map(JSON.parse)
                $.each(data, function (key, val) {
                    var ev = $.extend(new entities.event, val);
                    cevents.push(ev);
                })
            }
            loadEvents(cevents);
        }
        );
        //return cevents;
    },
    _getResources: function (siteid, loadResources) {
        var cresources = [];
        restapi._get('resources/' + siteid, function (data) {
            if (data.length > 0) {
                //var items = data.map(JSON.parse)
                $.each(data, function (key, val) {
                    var ev = $.extend(new entities.resource, val);
                    cresources.push(ev);
                })
            }
            loadResources(cresources);
            //cresources = data;
        })
        //return cresources;
    },
    _getSites: function (loadSites) {
        var csites = [];
        restapi._get('sites', function (data) {

            if (data.length > 0) {
                //var items = data.map(JSON.parse)
                $.each(data, function (key, val) {
                    var ev = $.extend(new entities.site, val);
                    csites.push(ev);
                })
            };
            loadSites(csites);
        })
        //  return csites;
    },
    _createActivity: function (data, successfn) {
        restapi._post('activity/create', JSON.stringify(data), successfn);
    },
    _updateActivity: function (data, successfn) {
        restapi._post('activities/update', JSON.stringify(data), successfn);
    },
    _updateEvent: function (data, successfn) {
        restapi._post('event/update', JSON.stringify(data), successfn);
    },
    _updateActivities: function (data, successfn) {
        restapi._post('activities/update', JSON.stringify(data), successfn);
    },
    _cancelActivity: function (data, successfn) {
        restapi._post('activities/cancel', JSON.stringify(data), successfn);
    },
    _getOppoUrl: function (eventId, localEvents) {
        //<http://mycrm/myOrg/main.aspx?etc=4&id=%7b899D4FCF-F4D3-E011-9D26-00155DBA3819%7d&pagetype=entityrecord>
        var oppId = "";
        $.each(localEvents, function (key, val) {
            if (val.id == eventId) {
                oppId = val.oppoid;
                return;
            }
        });
        if (oppId == null || oppId == undefined || oppId =="") {
            alert("No Event exist!");
            return null;
        }
        var orgUrl = GetGlobalContext().getClientUrl();
        return orgUrl + "/main.aspx?etc=3&id=%7b" + oppId + "%7d&pagetype=entityrecord";
    },
    _isRetailHold: function (eventId, localEvents) {
        var isRetail = false;
        $.each(localEvents, function (key, val) {
            if (val.id == eventId && val.isretailhold) {
                isRetail = true;
                return;
            }
        });
        return isRetail;
    },
    _isEventHold: function (eventId, localEvents) {
        var isEvent = false;
        $.each(localEvents, function (key, val) {
            if (val.id == eventId && val.iseventhold) {
                isEvent = true;
                return;
            }
        });
        return isEvent;
    },
    _isLeague: function (eventId, localEvents) {
        var isLeague = false;
        $.each(localEvents, function (key, val) {
            if (val.id == eventId && val.isleague) {
                isLeague = true;
                return;
            }
        });
        return isLeague;
    },
    _getRelated: function (eventId, localEvents) {
        var oppId = "";
        var relatedEvents = [];
        $.each(localEvents, function (key, val) {
            if (val.id == eventId) {
                oppId = val.oppoid;
                return;
            }
        });
        $.each(localEvents, function (key, val) {
            if (val.oppoid == oppId && val.oppoid != undefined && val.oppoid != "" && val.oppoid != null) {
                relatedEvents.push(val);
            }
        });
        return relatedEvents;
    }
}