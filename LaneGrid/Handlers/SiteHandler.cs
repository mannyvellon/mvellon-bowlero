﻿using LaneGrid.Models;
using LaneGrid.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Handlers
{
    public interface ISiteHandler
    {
        Site[] GetSites();
        Site GetSite(string id);
    }

    public class SiteHandler: ISiteHandler
    {
        private IStore store;

        public SiteHandler(IStore store)
        {
            this.store = store;
        }

        public Site[] GetSites()
        {
            return store.GetSites();
        }

        public Site GetSite(string id)
        {
            return store.GetSite(id);
        }
    }
}