﻿using System;

using LaneGrid.Models;
using System.Net;
using System.Collections.Generic;
using System.Configuration;
using LaneGrid.Storage;

namespace LaneGrid.Handlers
{
    
    public interface IActivityHandler
    {
        int Add(string siteid, Activity[] activities);
        Activity Create(Activity a);

        Activity Get(string id);
        Activity[] Get(string siteid, string sdate, string edate, string userid);
        Activity[] GetForEvent(string idEvent);
        void Update(Activity[] activities);
        void Cancel(Activity a);
    }

    public class ActivityHandler : IActivityHandler
    {
        private IStore store;

        public ActivityHandler(IStore store)
        {
            this.store = store;
        }

        public int Add(string siteid, Activity[] activities)
        {
            return store.StoreActivities(activities, siteid);
        }

        public Activity Create(Activity a)
        {
            a.id = Guid.NewGuid().ToString();
            store.StoreActivities(new Activity[] { a }, a.siteid);
            return a;
        }

        public Activity Get(string id)
        {
            return store.GetActivity(id);
        }

        public Activity[] Get(string siteid, string sdate, string edate, string userid)
        {
            return store.GetActivities(siteid, sdate, edate, userid);
        }

        public Activity[] GetForEvent(string idEvent)
        {
            return store.GetActivitiesForEvent(idEvent);
        }

        public void Update(Activity[] activities)
        {
            store.UpdateActivities(activities);
        }

        public void Cancel(Activity a)
        {
            store.Cancel(a);
        }
    }
}
