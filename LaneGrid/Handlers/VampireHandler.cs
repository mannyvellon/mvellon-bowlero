﻿using System;
using System.Collections.Generic;
using System.Linq;
using LaneGrid.Models;
using System.Web;
using Microsoft.Xrm.Sdk;
using LaneGrid.Common;
using BAMF.MSCRM.BusinessEntities.Enums;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using LaneGrid.Storage;
using LaneGrid.Helpers;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;

namespace LaneGrid.Handlers
{
    public interface IVampireHandler
    {
        int VampireSites();
        int VampireActivities(string siteid, string startDate, string endDate);
        int VampireResources(string siteid);

    }

    public class VampireHandler : IVampireHandler
    {
        private IStore store;

        public VampireHandler(IStore store)
        {
            this.store = store;
        }

        public int VampireSites() 
        {
            // Fetch sites from Proxy
            string uri = ConfigurationManager.AppSettings["crmProxyString"] + "sites/getallcenters";
            WebRequest req = WebRequest.Create(uri);
            WebResponse res = req.GetResponse();
            using (Stream dataStream = res.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);

                // Read the content.
                string responseFromServer = reader.ReadToEnd();

                Site[] sites = JsonConvert.DeserializeObject<Site[]>(responseFromServer);
                store.StoreSites(sites);
                return sites.Length;
            }


        }

        public int VampireActivities(string siteid, string startDate, string endDate)
        {
            // get entertainment centerid for site
            Site site = store.GetSite(siteid);
            if (site == null)
                throw new Exception("Invalid siteid");
            string ecId = site.EntertainmentCenterId;

            DateTime s = DateTime.Parse(startDate);
            DateTime e = DateTime.Parse(endDate);
            IOrganizationService clientService = CrmService.GetInstance();

            int page = 1;
            string cookie = "";
            int total = 0;
            while (true)
            {
                EntityCollection events = clientService
                                    .RetrieveMultiple(new FetchExpression(string.Format(Constants.FetchXmls.ServiceActivityRetrieveMultipleFetch
                                    , ecId, s, e, "1000", System.Security.SecurityElement.Escape(cookie), page.ToString())));
                if (events != null && events.Entities.Count > 0)
                {
                    Activity[] acts = GetActivityList(events);
                    store.StoreActivities(acts, siteid);

                    total += events.Entities.Count;

                }

                if (events.MoreRecords)
                {
                    page++;
                    cookie = events.PagingCookie;
                }
                else
                    break;
            }

            return total;

        }

        public int VampireResources(string siteid)
        {
            // get entertainment centerid for site
            Site site = store.GetSite(siteid);
            if (site == null)
                throw new Exception("Invalid siteid");
            string ecId = site.EntertainmentCenterId;

            // retrieve the resources for this site
            CrmServiceClient clientService = CrmService.GetClientInstance();
            EntityCollection colResources = clientService.GetEntityDataByFetchSearchEC
                           (string.Format(Constants.FetchXmls.ResourceRetrieveFetch, ecId));

            // convert them to LaneGrid objects and figure out hierarcy
            List<ResourceTree> resources = ConvertLResourceToList(colResources);

            // find the parent node(s)
            IEnumerable<ResourceTree> parentRes = resources.Where(p => p.parentid == null);

            // TODO: remove warning
            if (parentRes.Count() > 1)
                System.Diagnostics.Debug.WriteLine("Eek. Site " + siteid + " has multiple root-level resources");

            // create the composite list
            List<ResourceTree> hierarchicalRes = new List<ResourceTree>();

            // iterate through each parent node and find the children
            foreach (ResourceTree pRes in parentRes)
            {
                RelateChildren(resources, pRes);

                // add to the list
                hierarchicalRes.Add(pRes);
            }

            // store this information
            // TODO: support multiple root resource trees!
            store.StoreCenterResources(siteid, hierarchicalRes[0]);
            return hierarchicalRes.Count;
        }

        /*
         * Computes the "children" field by "resolving" the references to other items in the list
         * 
         * Note: this is a recursive function
         */
        private void RelateChildren(List<ResourceTree> resources, ResourceTree parent)
        {
            string parentId = parent.id;

            // Find elements in the list that refer to the current one as their parent
            List<ResourceTree> children = resources.Where(p => p.parentid == parentId).ToList();
            if (children.Count() == 0)
                // if no children, just return
                return;

            // Process the children recursively
            for (int i = 0; i < children.Count; i++)
                RelateChildren(resources, children[i]);
   
            // set the children field
            parent.children = children.ToArray();
        }

        /*
         * Converts entities retrieved from Dynamics into a list of LaneGrid objects
         */
        private List<ResourceTree> ConvertLResourceToList(EntityCollection lstLResources)
        {
            // Create the return list. Use "ResourceTree" instead of "Resource" in order to better handle children
            List<ResourceTree> resources = new List<ResourceTree>();

            // iterate through each entity, copying fields as needed
            foreach (var ent in lstLResources.Entities)
            {
                ResourceTree resource = new ResourceTree();
                resource.id = ent.Id.ToString();
                resource.name = ent.Contains(Constants.Attributes.Resource.Name) ? ent[Constants.Attributes.Resource.Name].ToString() : "";
                resource.type = ent.Contains(Constants.Attributes.Resource.ResourceType) ?
                    ((ResourceType)(ent.GetAttributeValue<OptionSetValue>(Constants.Attributes.Resource.ResourceType).Value)).ToString()
                    : ResourceType.NotSpecified.ToString();

                // Handle some BS
                if (resource.type.ToLower().Contains("room"))
                {
                    resource.type = "Room";
                }

                if (ent.Contains(Constants.Attributes.Resource.Floor))
                {
                    resource.parentid = ent.GetAttributeValue<EntityReference>(Constants.Attributes.Resource.Floor).Id.ToString();
                }

                if (ent.Contains(Constants.Attributes.Resource.Room))
                {
                    resource.parentid = ent.GetAttributeValue<EntityReference>(Constants.Attributes.Resource.Room).Id.ToString();
                
                }
                resource.expanded = true;
                resources.Add(resource);
            }

            // return the list
            return resources;
        }

        private Activity[] GetActivityList(EntityCollection entities)
        {

            List<Activity> activities = new List<Activity>();
            //select the first record
            foreach (var entity in entities.Entities)
            {
                if (entity.Contains(Constants.Attributes.Activity.Resources) && ((EntityCollection)entity[Constants.Attributes.Activity.Resources]).Entities.Count > 0)
                {
                    Entity eResource = ((EntityCollection)entity[Constants.Attributes.Activity.Resources]).Entities[0];
                    //check if eResource is already added to collection;
                    var resourceId = ((EntityReference)eResource["partyid"]).Id;
                    Activity activity = new Activity
                    {
                        id = entity.Id.ToString(),
                        start = entity.Contains(Constants.Attributes.Activity.ScheduledStart) ? entity.GetAttributeValue<DateTime>(Constants.Attributes.Activity.ScheduledStart).ToString("s") : DateTime.MinValue.ToString("s"), //35-Est
                        end = entity.Contains(Constants.Attributes.Activity.ScheduledEnd) ? entity.GetAttributeValue<DateTime>(Constants.Attributes.Activity.ScheduledEnd).ToString("s") : DateTime.MinValue.ToString("s"), //35-Est

                        subject = entity.Contains(Constants.Attributes.Activity.Subject) ? entity.GetAttributeValue<string>(Constants.Attributes.Activity.Subject) : string.Empty,
                        text = entity.Contains(Constants.Attributes.Activity.Subject) ? entity.GetAttributeValue<string>(Constants.Attributes.Activity.Subject) : string.Empty,
                        guestcount = entity.Contains(Constants.Attributes.Activity.GuestCount) ? entity.GetAttributeValue<Int32>(Constants.Attributes.Activity.GuestCount).ToString()
                                                                                                    : entity.Contains(Constants.Attributes.Activity.EventExpctedNumber) ?
                                                                                                        ((int)((AliasedValue)entity[Constants.Attributes.Activity.EventExpctedNumber]).Value).ToString() : string.Empty,
                        totalrevenue = entity.Contains(Constants.Attributes.Activity.TotalRevenueFromActivity) ? entity.GetAttributeValue<Money>(Constants.Attributes.Activity.TotalRevenueFromActivity).Value.ToString("C")
                                                                                                    : entity.Contains(Constants.Attributes.Activity.TotalRevenue) ?
                                                                                                        ((Money)((AliasedValue)entity[Constants.Attributes.Activity.TotalRevenue]).Value).Value.ToString("C") : string.Empty,
                        onholddate = entity.Contains(Constants.Attributes.Activity.CreatedOn) ? entity.GetAttributeValue<DateTime>(Constants.Attributes.Activity.CreatedOn).ToShortDateString() : "", //35-Est
                        repname = entity.Contains(Constants.Attributes.Activity.RepName) ? ((EntityReference)((AliasedValue)entity[Constants.Attributes.Activity.RepName]).Value).Name.ToString() : string.Empty,
                        partyname = entity.Contains(Constants.Attributes.Activity.Customers) ? GetPartyName((EntityCollection)entity[Constants.Attributes.Activity.Customers]) : string.Empty,
                        isretailhold = entity.Contains(Constants.Attributes.Activity.IsRetailHold) ? (bool)entity[Constants.Attributes.Activity.IsRetailHold] : false,
                        iseventhold = entity.Contains(Constants.Attributes.Activity.IsEventHold) ? (bool)entity[Constants.Attributes.Activity.IsEventHold] : false,
                        resource = resourceId.ToString(),
                        createdbyname = entity.Contains(Constants.Attributes.Activity.CreatedBy) ? ((EntityReference)entity[Constants.Attributes.Activity.CreatedBy]).Name : string.Empty,
                        isbufferactivity = entity.Contains(Constants.Attributes.Activity.IsBufferActivity) ? (bool)entity[Constants.Attributes.Activity.IsBufferActivity] : false,
                        oppoStart = entity.Contains(Constants.Attributes.Activity.OppStart) ? ((DateTime)(entity.GetAttributeValue<AliasedValue>(Constants.Attributes.Activity.OppStart).Value)).ToString("s") : DateTime.MinValue.ToString("s"), //35-Est
                        oppoEnd = entity.Contains(Constants.Attributes.Activity.OppEnd) ? ((DateTime)(entity.GetAttributeValue<AliasedValue>(Constants.Attributes.Activity.OppEnd).Value)).ToString("s") : DateTime.MinValue.ToString("s"), //35-Est
                    };
                    // check if LMA data exists
                    if (entity.Contains(Constants.Attributes.Activity.LeagueName))
                    {
                        activity.leaguename = entity.GetAttributeValue<string>(Constants.Attributes.Activity.LeagueName);
                        activity.text = " League Name : " + activity.leaguename;
                    }
                    if (entity.Contains(Constants.Attributes.Activity.LeagueType))
                    {
                        activity.leaguetype = entity.GetAttributeValue<string>(Constants.Attributes.Activity.LeagueType);
                        activity.text = activity.text + " League Type : " + activity.leaguetype;
                    }
                    // check the status of event
                    if (entity.Contains(Constants.Attributes.Activity.RegardingObjectId))
                    {
                        switch ((EventStatus)((OptionSetValue)entity.GetAttributeValue<AliasedValue>(Constants.Attributes.Activity.OpportunityStatus).Value).Value)
                        {
                            case EventStatus.Definite:
                            case EventStatus.Complete:
                                activity.backColor = "#98FB98";
                                break;
                            case EventStatus.Tentative:
                                activity.backColor = "#FFFF7F";
                                break;
                            case EventStatus.Prospective:
                                activity.backColor = "#FF9393";
                                break;
                            default:
                                activity.backColor = "#FFFF7F";
                                break;
                        }
                        activity.oppoid = entity.GetAttributeValue<EntityReference>(Constants.Attributes.Activity.RegardingObjectId).Id.ToString();
                    }
                    else if (entity.Contains(Constants.Attributes.Activity.ClientID))
                    {
                        var leagueType = entity.GetAttributeValue<string>(Constants.Attributes.Activity.LeagueType);
                        if (leagueType == "Tournament")
                        {
                            activity.backColor = "#F4A460";

                        }
                        else
                        {
                            activity.backColor = "#CDE7F0";
                        }
                        //also set the league attribute
                        activity.isleague = true;
                    }
                    if (activity.isretailhold)
                    {
                        activity.backColor = "#D3D3D3";
                    }
                    if (activity.iseventhold)
                    {
                        activity.backColor = "#b19cd9";
                    }
                    if (activity.isbufferactivity)
                    {
                        activity.oppoid = entity.Contains(Constants.Attributes.Activity.RegardingObjectId) ? entity.GetAttributeValue<EntityReference>(Constants.Attributes.Activity.RegardingObjectId).Id.ToString() : Guid.Empty.ToString();
                        activity.backColor = "#f5fffa";
                    }
                    activities.Add(activity);
                }
            }

            return activities.ToArray();
        }

        private string GetPartyName(EntityCollection collection)
        {
            if (collection.Entities.Count > 0)
            {
                return ((EntityReference)collection[0][Constants.Attributes.ActivityParty.PartyId]).Name;
            }
            return string.Empty;
        }

    }
}