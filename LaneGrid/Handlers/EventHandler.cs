﻿using BAMF.MSCRM.BusinessEntities;
using System;
using System.Collections.Generic;
using Microsoft.Xrm.Tooling.Connector;
using LaneGrid.Helpers;
using Microsoft.Xrm.Sdk;
using LaneGrid.Common;
using BAMF.MSCRM.BusinessEntities.Enums;
using System.Reflection;
using Microsoft.Xrm.Sdk.Query;
using LaneGrid.Storage;

namespace LaneGrid.Handlers
{

    public interface IEventHandler
    {
        Event GetFromGems(string id);
        Models.Event Get(string id);
        void Update(Models.Activity a);
    }

    public class EventHandler : IEventHandler
    {
        private IStore store;

        public EventHandler(IStore store)
        {
            this.store = store;
        }

        public Models.Event Get(string id)
        {
            return store.GetEvent(id, true);
        }

        /*
         * Note that this method returns very minimal information about an event. More is
         * available, it's just not returned!
         */
        public Event GetFromGems(string id)
        {
            CrmServiceClient csc = CrmService.GetClientInstance();
            EntityCollection events =
                csc.GetEntityDataByFetchSearchEC(
                    string.Format(Constants.FetchXmls.OpportunityRetrieveFetch, id));

            if (events.Entities.Count > 0)
            {
                //select the first record
                Entity opportunity = events.Entities[0];
                Event ev = new Event
                {
                    Name = opportunity.Contains(Constants.Attributes.Event.Name) ? opportunity[Constants.Attributes.Event.Name].ToString() : string.Empty,
                    Id = opportunity.Id,

                };
                EventStatus status;
                if (opportunity.Contains(Constants.Attributes.Event.StatusCode))
                {
                    Enum.TryParse<EventStatus>(((OptionSetValue)opportunity[Constants.Attributes.Event.StatusCode]).Value.ToString(), out status);
                    ev.Status = (ServiceActivityStatus)status;
                }
                EventState state;
                if (opportunity.Contains(Constants.Attributes.Event.StateCode))
                {
                    Enum.TryParse<EventState>(((OptionSetValue)opportunity[Constants.Attributes.Event.StateCode]).Value.ToString(), out state);
                    ev.State = (ServiceActivityState)state;
                }

            }
            return null;
        }

        public void Update(Models.Activity activity)
        {
            // TODO: Communicate with a CRM Proxy (new endpoint) to update the event
            // as per activity.oppoid, activity.start and activity.end
        }
    }
}
