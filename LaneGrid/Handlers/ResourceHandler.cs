﻿using System;

using LaneGrid.Models;
using System.Net;
using System.Collections.Generic;
using System.Configuration;
using LaneGrid.Storage;
using BAMF.MSCRM.BusinessEntities;
using Resource = LaneGrid.Models.Resource;
using Microsoft.Xrm.Sdk;
using LaneGrid.Helpers;
using Microsoft.Xrm.Sdk.Query;
using LaneGrid.Common;
using System.Linq;
using BAMF.MSCRM.BusinessEntities.Enums;
using Newtonsoft.Json;
using Renci.SshNet.Security;
using Microsoft.Ajax.Utilities;
using System.Globalization;

namespace LaneGrid.Handlers
{
    
    public interface IResourceHandler
    {
        void AddCenterResources(string centernum, ResourceTree resourceTree);
        ResourceTree GetCenterResources(string centernum);
        List<string> GetCenterLaneResourceIds(string siteid);
        List<string> GetLmaCenterLaneResourceIds(string siteid);
        Resource GetResource(string id);
        AvailableSlotsResponse GetAvailableSlots(Guid productId, Guid ecId, string startDate, Guid eventTypeId, string selectedNoOfPeople, string HoursRequired, bool isAlaCarte = false);
        object GetResourceAvailability(string resourceId, string startdate, string enddate);
    }

    public class ResourceHandler : IResourceHandler
    {
        private IStore store;
        private const int secondsToStoreProductResourceInCache = 3600;      // one hour

        public ResourceHandler(IStore store)
        {
            this.store = store;
        }

        public void AddCenterResources(string centernum, ResourceTree resourceTree)
        {
            store.StoreCenterResources(centernum, resourceTree);
        }

        public AvailableSlotsResponse GetAvailableSlots(Guid productId, Guid ecId, string startDate, Guid eventTypeId, string selectedNoOfPeople, string HoursRequired, bool isAlaCarte = false)
        {
            // adjust to start of day
            // TODO: review if it's ok to hard-code this
            DateTime startApiDate = Convert.ToDateTime(startDate).AddHours(9);

            // if we're looking at today, adjust to two days from now (special case)
            // TODO: review - this seems stupid
            DateTime currentDate = DateTime.Now.Date;
            if (startApiDate.Date == currentDate.Date)
                startApiDate = startApiDate.AddDays(2);

            // calculate end of month and then one more day
            DateTime endApiDate = new DateTime(startApiDate.Year, startApiDate.Month, DateTime.DaysInMonth(startApiDate.Year, startApiDate.Month));
            endApiDate = endApiDate.AddDays(1);

            // fetch the site id for the venue
            Guid siteId = Guid.Empty;
            foreach (LaneGrid.Models.Site s in store.GetSites())
                if (s.EntertainmentCenterId == ecId.ToString())
                {
                    siteId = Guid.Parse(s.Id);
                    break;
                }


            // lookup product resources associated with the given product id at this venue. First, see if cached
            int numberOfPeople = selectedNoOfPeople != null ? int.Parse(selectedNoOfPeople) : 0;
            ProductResource pr = GetProductResources(ecId, productId, numberOfPeople, isAlaCarte);

            AvailableSlotsResponse availableSlotResponse = new AvailableSlotsResponse();

            // TODO: should be able to cache these
            Schedules schedules = GetSchedules(ecId, eventTypeId);

            // Getting all resources of site
            List<BAMF.MSCRM.BusinessEntities.Resource> allResources = GetResourcesOfTypeAtCenter(siteId, pr.resourceType);

            // Getting all activities
            LaneGrid.Models.Activity[] activities = store.GetActivities(    siteId.ToString(), 
                                                                            startApiDate.ToString("yyyy-MM-dd"), 
                                                                            endApiDate.ToString("yyyy-MM-dd"), 
                                                                            null);

            int j = 0;
            List<DaySlotDetail> daySlots = new List<DaySlotDetail>();
            List<DaySchedule> daySchedules = new List<DaySchedule>();
            List<AvailbleSlot> availableSlots = new List<AvailbleSlot>();
            DaySlotDetail daySlotObj = new DaySlotDetail();
            List<DaySchedule> matchedSchdules = new List<DaySchedule>();
            for (DateTime i = startApiDate.Date; i <= endApiDate.Date; i = i.Date.AddDays(1))
            {
                DateTime selectedDate = startApiDate.AddDays(j);
                daySlotObj.Date = selectedDate.ToString("yyyy-MM-dd");

                // Event Type          
                if (schedules.EventTypeSchedules.Where(d => d.DayOfWeek == selectedDate.DayOfWeek.ToString()).Any())
                {
                    CheckSlots(schedules.EventTypeSchedules, selectedDate, siteId, pr.resourceQuantity, availableSlots, allResources, activities, daySchedules, HoursRequired);
                } // Holiday 
                else if (schedules.HolidayTypeSchedules.Where(p => p.StartDate <= selectedDate.Date && p.EndDate >= selectedDate.Date && p.DayOfWeek == selectedDate.DayOfWeek.ToString()).Any())
                {
                    CheckSlots(schedules.HolidayTypeSchedules, selectedDate, siteId, pr.resourceQuantity, availableSlots, allResources, activities, daySchedules, HoursRequired);

                } // Normal
                else if (schedules.NormalTypeSchedules.Where(d => d.DayOfWeek == selectedDate.DayOfWeek.ToString()).Any())
                {
                    CheckSlots(schedules.NormalTypeSchedules, selectedDate, siteId, pr.resourceQuantity, availableSlots, allResources, activities, daySchedules, HoursRequired);
                }
                j++;

            }
            // Filter available slots considering center closure times
            var lstFilteredAvailableSlots = new List<AvailbleSlot>();
            try
            {
                foreach (var item in availableSlots.OrderBy(a => a.AvailableDate))
                {
                    var slotStart = item.StartTime;
                    var slotEnd = item.StartTime.AddHours(Convert.ToDouble(HoursRequired)).AddMinutes(-1);
                    var slotDaySchedules = daySchedules.Where(d => d.StartTime.Value.Date == item.StartTime.Date).ToList();
                    var slotNextDaySchedule = daySchedules.Where(d => d.StartTime.Value.Date == item.StartTime.Date.AddDays(1)).OrderBy(d => d.StartTime).FirstOrDefault();
                    foreach (var slotDaySchedule in slotDaySchedules.OrderBy(d => d.StartTime))
                    {
                        var overlapSchedule = false;
                        if (slotDaySchedule != null && slotNextDaySchedule != null && slotDaySchedule.EndTime.Value.Ticks + TimeSpan.TicksPerMinute == slotNextDaySchedule.StartTime.Value.Ticks)
                        {
                            overlapSchedule = true;
                        }
                        if (overlapSchedule && slotNextDaySchedule != null)
                        {
                            if (slotStart >= slotDaySchedule.StartTime && slotEnd <= slotNextDaySchedule.EndTime)
                            {

                                lstFilteredAvailableSlots.Add(item);
                            }
                            else
                            {
                                // dont do anything as its a invalid avaialbility
                            }

                        }
                        else
                        {
                            if (slotStart >= slotDaySchedule.StartTime && slotEnd <= slotDaySchedule.EndTime)
                            {
                                lstFilteredAvailableSlots.Add(item);
                            }
                            else
                            {
                                // dont do anything as its a invalid availability
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            // reset start and end dates
            startApiDate = Convert.ToDateTime(startDate).AddHours(9);
            if (startApiDate.Date == currentDate.Date)
            {
                startApiDate = startApiDate.AddDays(2);
            }
            endApiDate = new DateTime(startApiDate.Year, startApiDate.Month, DateTime.DaysInMonth(startApiDate.Year, startApiDate.Month));
            endApiDate = endApiDate.AddDays(1);
            j = 0;
            for (DateTime i = startApiDate.Date; i <= endApiDate.Date; i = i.AddDays(1))
            {
                #region Dates converting to Json
                daySlotObj = new DaySlotDetail();
                foreach (var slot in lstFilteredAvailableSlots)
                {
                    if (slot.AvailableDate.Date == i.Date)
                    {
                        daySlotObj.IsSlotAvailable = true;
                        daySlotObj.Date = slot.AvailableDate.ToString("yyyy-MM-dd");
                        #region timeslots
                        switch (slot.StartTime.ToString("HH:mm"))
                        {
                            case "09:00":
                                daySlotObj.AM9 = new KeyValuePair<bool, string>(true, "09:00");
                                break;
                            case "09:30":
                                daySlotObj.AM930 = new KeyValuePair<bool, string>(true, "09:30");
                                break;
                            case "10:00":
                                daySlotObj.AM10 = new KeyValuePair<bool, string>(true, "10:00");
                                break;
                            case "10:30":
                                daySlotObj.AM1030 = new KeyValuePair<bool, string>(true, "10:30");
                                break;
                            case "11:00":
                                daySlotObj.AM11 = new KeyValuePair<bool, string>(true, "11:00");
                                break;
                            case "11:30":
                                daySlotObj.AM1130 = new KeyValuePair<bool, string>(true, "11:30");
                                break;
                            case "12:00":
                                daySlotObj.PM12 = new KeyValuePair<bool, string>(true, "12:00");
                                break;
                            case "12:30":
                                daySlotObj.PM1230 = new KeyValuePair<bool, string>(true, "12:30");
                                break;
                            case "13:00":
                                daySlotObj.PM1 = new KeyValuePair<bool, string>(true, "13:00");
                                break;
                            case "13:30":
                                daySlotObj.PM130 = new KeyValuePair<bool, string>(true, "13:30");
                                break;
                            case "14:00":
                                daySlotObj.PM2 = new KeyValuePair<bool, string>(true, "14:00");
                                break;
                            case "14:30":
                                daySlotObj.PM230 = new KeyValuePair<bool, string>(true, "14:30");
                                break;
                            case "15:00":
                                daySlotObj.PM3 = new KeyValuePair<bool, string>(true, "15:00");
                                break;
                            case "15:30":
                                daySlotObj.PM330 = new KeyValuePair<bool, string>(true, "15:30");
                                break;
                            case "16:00":
                                daySlotObj.PM4 = new KeyValuePair<bool, string>(true, "16:00");
                                break;
                            case "16:30":
                                daySlotObj.PM430 = new KeyValuePair<bool, string>(true, "16:30");
                                break;
                            case "17:00":
                                daySlotObj.PM5 = new KeyValuePair<bool, string>(true, "17:00");
                                break;
                            case "17:30":
                                daySlotObj.PM530 = new KeyValuePair<bool, string>(true, "17:30");
                                break;
                            case "18:00":
                                daySlotObj.PM6 = new KeyValuePair<bool, string>(true, "18:00");
                                break;
                            case "18:30":
                                daySlotObj.PM630 = new KeyValuePair<bool, string>(true, "18:30");
                                break;
                            case "19:00":
                                daySlotObj.PM7 = new KeyValuePair<bool, string>(true, "19:00");
                                break;
                            case "19:30":
                                daySlotObj.PM730 = new KeyValuePair<bool, string>(true, "19:30");
                                break;
                            case "20:00":
                                daySlotObj.PM8 = new KeyValuePair<bool, string>(true, "20:00");
                                break;
                            case "20:30":
                                daySlotObj.PM830 = new KeyValuePair<bool, string>(true, "20:30");
                                break;
                            case "21:00":
                                daySlotObj.PM9 = new KeyValuePair<bool, string>(true, "21:00");
                                break;
                            case "21:30":
                                daySlotObj.PM930 = new KeyValuePair<bool, string>(true, "21:30");
                                break;
                            case "22:00":
                                daySlotObj.PM10 = new KeyValuePair<bool, string>(true, "22:00");
                                break;
                            case "22:30":
                                daySlotObj.PM1030 = new KeyValuePair<bool, string>(true, "22:30");
                                break;
                            case "23:00":
                                daySlotObj.PM11 = new KeyValuePair<bool, string>(true, "23:00");
                                break;
                            case "23:30":
                                daySlotObj.PM1130 = new KeyValuePair<bool, string>(true, "23:30");
                                break;
                            default:

                                break;
                        }
                        #endregion
                    }
                }
                #endregion

                if (daySlotObj.Date == null)
                    daySlotObj.Date = startApiDate.AddDays(j).ToString("yyyy-MM-dd");

                j++;

                daySlots.Add(daySlotObj);
            }


            availableSlotResponse.MaxCapacity = pr.maxCapacity;
            availableSlotResponse.SlotDetails = daySlots;

            return availableSlotResponse;

        }

        private Schedules GetSchedules(Guid ecId, Guid eventTypeId)
        {
            Schedules s = null;
            string cacheKey = string.Format("scheduleForSite-{0}-eventType-{1}", ecId.ToString(), eventTypeId.ToString());

            if (store.IsCached(cacheKey))
            {
                string json = store.GetFromCache(cacheKey);
                s = JsonConvert.DeserializeObject<Schedules>(json);
            }
            else
            {
                // no. get from Dynamics
                IOrganizationService _orgService = CrmService.GetInstance();

                s = new Schedules();

                // convert schedules from Dynamics entities to C# objects
                EntityCollection allEntities = _orgService.RetrieveMultiple(new FetchExpression(string.Format(Constants.FetchXmls.CentreHoursFetch, ecId, eventTypeId)));

                s.DailySchedules = ConvertToSchedules(allEntities);
                s.EventTypeSchedules = s.DailySchedules.Where(p => p.ScheduleType==(int)ScheduleType.EventType).ToList();
                s.HolidayTypeSchedules = s.DailySchedules.Where(p => p.ScheduleType == (int)ScheduleType.Holiday).ToList();
                s.NormalTypeSchedules = s.DailySchedules.Where(p => p.ScheduleType == (int)ScheduleType.Normal).ToList();

                // cache it
                string json = JsonConvert.SerializeObject(s);
                store.StoreInCache(cacheKey, json, secondsToStoreProductResourceInCache);
            }

            return s;
        }

        private List<Schedule> ConvertToSchedules(EntityCollection allEntities)
        {
            List<Schedule> list = new List<Schedule>();
            foreach (Entity entity in allEntities.Entities)
            {
                Schedule s = new Schedule();
                s.DayOfWeek =DayOfWeek(entity);
                s.ScheduleType = ((OptionSetValue)((AliasedValue)entity.Attributes[Constants.Attributes.HoursSchedule.ScheduleType]).Value).Value;
                s.StartDate = ((DateTime)entity.GetAttributeValue<AliasedValue>(Constants.Attributes.HoursSchedule.StartDate).Value).Date;
                s.EndDate = ((DateTime)entity.GetAttributeValue<AliasedValue>(Constants.Attributes.HoursSchedule.EndDate).Value).Date;
                s.StartHour = entity.GetAttributeValue<OptionSetValue>(Constants.Attributes.DailySchedule.StartHours).Value - 100000000;
                s.StartMinute = entity.GetAttributeValue<OptionSetValue>(Constants.Attributes.DailySchedule.StartMinutes).Value - 100000000;
                s.EndHour = entity.GetAttributeValue<OptionSetValue>(Constants.Attributes.DailySchedule.EndHours).Value - 100000000;
                s.EndMinute = entity.GetAttributeValue<OptionSetValue>(Constants.Attributes.DailySchedule.EndMinutes).Value - 100000000;
                s.IsTierTime = entity.GetAttributeValue<bool>(Constants.Attributes.DailySchedule.IsTierTime);

                list.Add(s);
            }

            return list;
        }

        private ProductResource GetProductResources(Guid ecId, Guid productId, int numberOfPeople, bool isAlaCarte)
        {
            // lookup product resources associated with the given product id at this venue. First, see if cached
            string cacheKey = string.Format("resourcesForProduct-{0}-AtCenter-{1}", productId, ecId);
            ProductResource pr = null;

            // do we have this cached?
            if (store.IsCached(cacheKey))
            {
                // retrieve
                string json = store.GetFromCache(cacheKey);
                pr = JsonConvert.DeserializeObject<ProductResource>(json);
            }
            else
            {
                // no. get from Dynamics
                IOrganizationService _orgService = CrmService.GetInstance();

                // Get Resource Type
                EntityCollection allProductResources = null;
                if (isAlaCarte)
                    allProductResources = _orgService.RetrieveMultiple(new FetchExpression(string.Format(Constants.FetchXmls.ProductResourceAlaCarteFetch, productId.ToString(), ecId)));
                else
                    allProductResources = _orgService.RetrieveMultiple(new FetchExpression(string.Format(Constants.FetchXmls.ProductResourceFetch, productId.ToString(), ecId)));

                if (allProductResources == null || allProductResources.Entities.Count == 0 || !allProductResources[0].Contains(Constants.Attributes.ProductResource.ResourceType))
                    // empty response
                    return null;

                // get capacity info 
                pr =  new ProductResource()
                {
                    resourceQuantity = allProductResources.Entities[0].Contains(Constants.ResourceProduct.MaxCapacity) ? Convert.ToInt32(Math.Ceiling(numberOfPeople / (decimal)allProductResources.Entities[0].GetAttributeValue<Int32>(Constants.ResourceProduct.MaxCapacity))) : numberOfPeople,
                    resourceType = allProductResources.Entities[0].GetAttributeValue<OptionSetValue>(Constants.Attributes.ProductResource.ResourceType).Value,
                    maxCapacity = allProductResources.Entities[0].GetAttributeValue<int>(Constants.Attributes.ProductResource.MaxCapacity)
                };

                // add to cache
                string json = JsonConvert.SerializeObject(pr);
                store.StoreInCache(cacheKey, json, secondsToStoreProductResourceInCache);
            }

            return pr;
        }

        private List<BAMF.MSCRM.BusinessEntities.Resource> GetResourcesOfTypeAtCenter(Guid siteId, int resourceType)
        {
            List<BAMF.MSCRM.BusinessEntities.Resource> list;

            // check cache
            string cacheKey = string.Format("resourcesOfType-{0}-atCenter-{1}", resourceType, siteId);
            if (store.IsCached(cacheKey))
            {
                string json = store.GetFromCache(cacheKey);
                list = JsonConvert.DeserializeObject<List<BAMF.MSCRM.BusinessEntities.Resource>>(json);
            }
            else
            {
                // no. get from Dynamics
                IOrganizationService _orgService = CrmService.GetInstance();

                List<Entity> resourcesCols = _orgService
                                .RetrieveMultiple(new FetchExpression(string.Format(Constants.FetchXmls.ResourceDetailsOLB,
                                siteId, resourceType))).Entities.ToList();
                list = resourcesCols.Select(ConvertToResource).ToList();

                // cache it
                string json = JsonConvert.SerializeObject(list);
                store.StoreInCache(cacheKey, json, secondsToStoreProductResourceInCache);
            }

            return list;
        }

        private void CheckSlots(List<Schedule> schedules, DateTime selectedDate, Guid siteId, int resourceQuantity, List<AvailbleSlot> availableSlots, List<BAMF.MSCRM.BusinessEntities.Resource> allResources, LaneGrid.Models.Activity[] activities, List<DaySchedule> daySchedules, string hoursRequired)
        {
            DateTime? startDateTime = null;
            DateTime? endDateTime = null;

            double hoursRequireddbl = 0;
            int minutesRequired = 0;
            if (!double.TryParse(hoursRequired, out hoursRequireddbl))
            {
                throw new Exception("Invalid Hours Requred in Check Slots");
            }
            minutesRequired = Convert.ToInt32(hoursRequireddbl * 60) - 1;

            IEnumerable<Schedule> tierSchedule = schedules.Where(t => t.IsTierTime);
            if (tierSchedule.Where(d => d.DayOfWeek == selectedDate.DayOfWeek.ToString()).Any())
            {
                foreach (Schedule tierData in tierSchedule)
                {
                    startDateTime = selectedDate.Date.AddHours(tierData.StartHour);
                    startDateTime = startDateTime.Value.AddMinutes(tierData.StartMinute);
                    daySchedules.Add(
                           new DaySchedule() { StartTime = startDateTime, EndTime = startDateTime.Value.AddHours(Convert.ToDouble(hoursRequired)) }
                           );
                    if (IsSlotAvailable(startDateTime.Value, startDateTime.Value.AddMinutes(minutesRequired), siteId, resourceQuantity, allResources, activities))
                    {
                        availableSlots.Add(
                             new AvailbleSlot()
                             {
                                 StartTime = startDateTime.Value,
                                 AvailableDate = startDateTime.Value
                             });
                    }
                }
            }
            else
            {
                List<Schedule> lstDaySchedule = schedules.Where(d => d.DayOfWeek == selectedDate.DayOfWeek.ToString()).ToList();
                foreach (Schedule daySchedule in lstDaySchedule)
                {


                    if (daySchedule != null)
                    {
                        // start date time
                        startDateTime = selectedDate.Date.AddHours(daySchedule.StartHour);
                        startDateTime = startDateTime.Value.AddMinutes(daySchedule.StartMinute);
                        // end date time
                        endDateTime = selectedDate.Date.AddHours(daySchedule.EndHour);
                        endDateTime = endDateTime.Value.AddMinutes(daySchedule.EndMinute);
                        daySchedules.Add(new DaySchedule() { StartTime = startDateTime, EndTime = endDateTime });
                        if (endDateTime.HasValue)
                        {
                            while (endDateTime > startDateTime)
                            {
                                //if (startDateTime.Value.AddHours(Convert.ToDouble(HoursRequired)) >= endDateTime)
                                //{
                                //    break;
                                //}
                                if (IsSlotAvailable(startDateTime.Value, startDateTime.Value.AddMinutes(minutesRequired), siteId, resourceQuantity, allResources, activities))
                                {
                                    availableSlots.Add(
                                         new AvailbleSlot()
                                         {
                                             StartTime = startDateTime.Value,
                                             AvailableDate = startDateTime.Value
                                         });
                                }
                                startDateTime = startDateTime.Value.AddMinutes(30);
                            }
                        }
                        else
                        {
                            if (IsSlotAvailable(startDateTime.Value, startDateTime.Value.AddMinutes(minutesRequired), siteId, resourceQuantity, allResources, activities))
                            {
                                availableSlots.Add(
                                     new AvailbleSlot()
                                     {
                                         StartTime = startDateTime.Value,
                                         AvailableDate = startDateTime.Value
                                     });
                            }
                        }

                    }
                }

            }
        }

        internal static bool IsSlotAvailable(DateTime startTime, DateTime endTime, Guid siteId, int resourceQuantity, List<BAMF.MSCRM.BusinessEntities.Resource> allResources, LaneGrid.Models.Activity[] activities)
        {

            List<Guid> lstResource = new List<Guid>();
            // Get Booked Resources
            IEnumerable<Guid> resources = GetBookedResources(activities, siteId, startTime, endTime);

            // Get Available Resources
            List<BAMF.MSCRM.BusinessEntities.Resource> availableResources = allResources.Where(p => (resources == null || !resources.Contains(p.Id))).ToList();

            // Get Available Lanes Order
            List<int> availableLanesOrder = availableResources.OrderBy(x => x.Order).Select(p => p.Order).ToList();
            List<int> allotedLanes = new List<int>();
            int numberOfLanesRqd = resourceQuantity;

            //find Best Lanes
            FindBestLanes(numberOfLanesRqd, numberOfLanesRqd, availableLanesOrder, ref allotedLanes);

            // Call the avalibility algoritham
            List<BAMF.MSCRM.BusinessEntities.Resource> allottedResources = availableResources.Where(p => (allotedLanes.Contains(p.Order))).ToList();
            if (allottedResources != null && allottedResources.Count > 0)
            {
                return true;
            }

            return false;

        }

        internal static void FindBestLanes(int finalLanesRequired, int noOfLanes, List<int> avalibleLanes, ref List<int> allottedLanes)
        {
            int foundSlot = 1; //List<int> 
            List<int> tempAllocation = new List<int>(); // List<int> allocatedSlots = new List<int>();
            for (int i = 0; i <= avalibleLanes.Count - 1; i++)
            {
                tempAllocation.Add(avalibleLanes[i]);
                if (foundSlot == noOfLanes) // if it is last avalible slot
                {
                    foreach (var item in tempAllocation)
                    {
                        allottedLanes.Add(item);
                    }

                    // check if all the slots are allocated as per required count
                    if (allottedLanes.Count != finalLanesRequired)
                    {
                        var tempNotAllocatedCount = finalLanesRequired - allottedLanes.Count;
                        // remove already allocatedslots from avalible slots and run the allocation alogrithm again
                        foreach (var item in allottedLanes)
                        {
                            avalibleLanes.Remove(item);
                        }
                        FindBestLanes(finalLanesRequired, tempNotAllocatedCount, avalibleLanes, ref allottedLanes);
                        return;
                    }
                    return;
                    // break; // break condition for recursion
                }
                if (avalibleLanes.Count - 1 != i)//check if this is not last element
                {
                    int diff = avalibleLanes[i + 1] - avalibleLanes[i];
                    if (diff == 1) //adjusent slot
                    {
                        foundSlot++;
                    }
                    else
                    {
                        //reset found slot to initial position
                        foundSlot = 1;
                        //clear the found array
                        tempAllocation.Clear();
                    }
                }
                else
                {
                    //clear the allocatedSlots as slots not found before last lane
                    tempAllocation.Clear();
                    if (noOfLanes >= 2)
                    {
                        //call recursive function
                        FindBestLanes(finalLanesRequired, noOfLanes - 1, avalibleLanes, ref allottedLanes);
                        return;
                    }
                }
            }
            return;
        }


        internal static IEnumerable<Guid> GetBookedResources(LaneGrid.Models.Activity[] activities, Guid siteId, DateTime startTime, DateTime endTime)
        {
            if (activities.Length == 0)
                return null;

            List<Guid> lstResource = new List<Guid>();

            string sstartTime = startTime.ToString("yyyy-MM-ddTHH:mm:ss");
            string sendTime =     endTime.ToString("yyyy-MM-ddTHH:mm:ss");
            foreach (Activity activity in activities)
            {
                if (activity.resource!=null)
                {
                    // there would only one resource would be associated with service activity;
                    // DateTime actStartDate = DateTime.ParseExact(activity.start, "yyyy-MM-ddTHH:mm:ss", ci);

                    // CRM gives activites for entire day, we need to filter it based on time
                    // DateTime actEndDate = DateTime.ParseExact(activity.end, "yyyy-MM-ddTHH:mm:ss", ci);

                    // check if act starttime is less than start time of event & act end time is greater than start time of event
                    //if ((actStartDate <= startTime && actEndDate > startTime) || (actStartDate >= startTime && actEndDate <= endTime)
                    //    || (actStartDate >= startTime && actStartDate <= endTime) && activity.resource!=null)

                    // TODO: Consider storing epoch times (ms since 1970) in the database in addition to string dates. That would allow
                    // dates to be compared without any lexical or DateTime-based calculation.

                    string astart = activity.start;
                    string aend = activity.end;

                    int c0 = astart.CompareTo(sstartTime);
                    if ( (c0<=0 && aend.CompareTo(sstartTime)>0) ||
                         (c0>=0 && aend.CompareTo(sendTime)<=0) ||
                         (c0>=0 && astart.CompareTo(sendTime)<=0) && activity.resource!=null)
                    {
                        Guid gres = Guid.Parse(activity.resource);
                        if (!lstResource.Contains(gres))
                            lstResource.Add(gres);
                    }
                }
            }
            return lstResource;
        }


        private  string DayOfWeek(Entity entity)
        {

            if (entity != null && entity.Contains(Constants.Attributes.DailySchedule.DayOfWeek))
            {
                int value = entity.GetAttributeValue<OptionSetValue>(Constants.Attributes.DailySchedule.DayOfWeek).Value;
                BAMF.MSCRM.BusinessEntities.Enums.DayOfWeek day = (BAMF.MSCRM.BusinessEntities.Enums.DayOfWeek)value;
                return day.ToString();
            }

            return string.Empty;
        }

        private  int GetHours(int value)
        {
            List<KeyValuePair<int, int>> hoursList = new List<KeyValuePair<int, int>>();
            for (int i = 0; i < 24; i++)
            {
                hoursList.Add(new KeyValuePair<int, int>(i, 100000000 + i));
            }

            if (hoursList.Count > 0)
            {
                foreach (var hh in hoursList)
                {
                    if (hh.Value == value)
                    {
                        return hh.Key;
                    }
                }
            }
            return 0;
        }

        private  int GetMinutes(int value)
        {
            List<KeyValuePair<int, int>> minutsList = new List<KeyValuePair<int, int>>();
            for (int i = 0; i < 60; i++)
            {
                minutsList.Add(new KeyValuePair<int, int>(i, 100000000 + i));
            }

            if (minutsList.Count > 0)
            {
                foreach (var mm in minutsList)
                {
                    if (mm.Value == value)
                    {
                        return mm.Key;
                    }
                }
            }
            return 0;
        }

        private  DateTime GetDate(Entity entity, string name)
        {
            if (entity != null && entity.Contains(name))
            {
                return ((DateTime)entity.GetAttributeValue<AliasedValue>(name).Value).Date;

            }

            return DateTime.Now.Date;
        }

        internal  BAMF.MSCRM.BusinessEntities.Resource ConvertToResource(Entity crmResource)
        {
            BAMF.MSCRM.BusinessEntities.Resource resource = new BAMF.MSCRM.BusinessEntities.Resource();
            resource.Id = crmResource.Id;
            resource.Name = crmResource.Contains(Constants.Resource.Name) ? crmResource[Constants.Resource.Name].ToString() : string.Empty;

            ResourceType rType; string ordLane = ""; string ordRoom = ""; string ordFloor = ""; string ordRoomParent = "";
            if (crmResource.Contains(Constants.Resource.ResourceType))
            {
                ;
                resource.TypeOfResource = (ResourceType)crmResource.GetAttributeValue<OptionSetValue>(Constants.Resource.ResourceType).Value;
                ordLane = crmResource.GetAttributeValue<int>(Constants.Resource.Order).ToString();
                //int.TryParse(,out ordLane);

            }
            if (crmResource.Contains(Constants.Resource.Room))
            {
                var room = crmResource[Constants.Resource.Room] as EntityReference;
                resource.Room = new BAMF.MSCRM.BusinessEntities.Resource
                {
                    Id = room.Id,
                    Name = room.Name
                };
                if (crmResource.Attributes.Contains("room." + Constants.Resource.Order))
                {
                    ordRoom = crmResource.GetAttributeValue<AliasedValue>("room." + Constants.Resource.Order).Value.ToString();
                    resource.Room.Order = int.Parse(ordRoom);
                }
                // ord room parent 
                if (crmResource.Attributes.Contains("roomParent." + Constants.Resource.Order))
                    ordRoomParent = crmResource.GetAttributeValue<AliasedValue>("roomParent." + Constants.Resource.Order).Value.ToString();
            }
            if (crmResource.Contains(Constants.Resource.Floor))
            {
                var floor = crmResource[Constants.Resource.Floor] as EntityReference;
                resource.Floor = new BAMF.MSCRM.BusinessEntities.Resource
                {
                    Id = floor.Id,
                    Name = floor.Name
                };
                if (crmResource.Attributes.Contains("floor." + Constants.Resource.Order))
                {
                    ordFloor = crmResource.GetAttributeValue<AliasedValue>("floor." + Constants.Resource.Order).Value.ToString();
                    resource.Floor.Order = int.Parse(ordFloor);
                }
                //int.TryParse(crmResource.GetAttributeValue<string>(Constants.Attributes.Resource.Order), out ordFloor);
                //resource.Order = ordFloor;
            }
            //check if room has parent
            string order = (ordRoomParent != "" ? ordRoomParent : ordFloor.ToString()) + ordRoom.ToString() + ordLane;
            if (order != "")
            {
                resource.Order = int.Parse(order);
            }
            return resource;
        }


        public ResourceTree GetCenterResources(string centernum)
        {
            return store.GetCenterResources(centernum);
        }
        public List<string> GetCenterLaneResourceIds(string ecId)
        {
            return store.GetCenterResourceIds(ecId);
        }

        public List<string> GetLmaCenterLaneResourceIds(string siteid)
        {
            return store.GetLmaCenterLaneResourceIds(siteid);
        }

        public Resource GetResource(string id)
        {
            return store.GetResource(id);
        }

        public object GetResourceAvailability(string resourceId, string startdate, string enddate)
        {
            throw new NotImplementedException();
        }
    }
}
