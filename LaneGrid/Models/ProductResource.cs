﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Models
{
    public class ProductResource
    {
        public int resourceType { get; set; }
        public int resourceQuantity { get; set; }
        public int maxCapacity { get; set; }

    }
}