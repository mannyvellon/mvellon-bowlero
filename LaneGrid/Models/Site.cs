﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Models
{
    public class Site
    {
        public string Id { get; set; }
        public string EntertainmentCenterId { get; set; }
        public string Name { get; set; }
        public int SiteNumber { get; set; }
    }
}