﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Models
{
    public class ResourceTree
    {
        public string name { set; get; }
        public string id { get; set; }
        public bool expanded { get; set; }
        public string type { get; set; }
        public ResourceTree[] children { get; set; }
        public string parentid { get; set; }
    }

    public class Resource
    {
        public string name { set; get; }
        public string id { get; set; }
        public bool expanded { get; set; }
        public string type { get; set; }
        public string parentid { get; set; }
    }
}