﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Models
{
    public class Schedule
    {
        public int ScheduleType { get; set; }
        public string DayOfWeek { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsTierTime { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
    }
}