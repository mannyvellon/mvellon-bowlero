﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Models
{
    public class Event
    {
        public string id { get; set; }
        public string centerid { get; set; }
        public List<Activity> activities { get; set; }
    }
}