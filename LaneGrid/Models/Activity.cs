﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaneGrid.Models
{
    public class Activity
    {
        public string id { get; set; }
        public string start { get; set; }
        public string end { get; set; }

        public string text { get; set; }
        public string resource { get; set; }
        public string cssClass { get; set; }
        public bool resizeDisabled { get; set; }
        public string siteid { get; set; }

        public string backColor { get; set; }
        public string oppoid { get; set; }
        public string repname { get; set; }
        public string guestcount { get; set; }
        public string onholddate { get; set; }
        public string totalrevenue { get; set; }
        public string userid { get; set; }

        public string partyname { get; set; }

        public bool isretailhold { get; set; }
        public bool iseventhold { get; set; }
        public string createdbyname { get; set; }

        public bool isbufferactivity { get; set; }

        public bool isleague { get; set; }
        public string leagueid { get; set; }
        public string leaguename { get; set; }
        public string leaguetype { get; set; }
        public string subject { get; set; }
        public string oppoStart { get; set; }
        public string oppoEnd { get; set; }

    }
}
