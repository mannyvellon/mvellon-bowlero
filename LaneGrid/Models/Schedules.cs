﻿using Microsoft.Ajax.Utilities;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Models
{
    public class Schedules
    {
        public List<Schedule> DailySchedules { get; set; }
        public List<Schedule> EventTypeSchedules { get; set; }
        public List<Schedule> HolidayTypeSchedules { get; set; }
        public List<Schedule> NormalTypeSchedules { get; set; }
    }
}