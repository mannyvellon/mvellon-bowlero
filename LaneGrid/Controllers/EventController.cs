﻿using BAMF.MSCRM.BusinessEntities;
using LaneGrid.Handlers;
using LaneGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LaneGrid.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EventController : ApiController
    {
        private IEventHandler handler = null;

        public EventController(IEventHandler handler)
        {
            this.handler = handler;
        }

        [HttpGet]
        [Route("event/fromgems/{id}")]
        public BAMF.MSCRM.BusinessEntities.Event GetGems(string id)
        {
            return handler.GetFromGems(id);
        }

        [HttpGet]
        [Route("event/get/{id}")]
        public Models.Event Get(string id)
        {
            return handler.Get(id);
        }

        [HttpPost]
        [Route("event/update")]
        public void Update([FromBody] Activity a)
        {
            handler.Update(a);
        }

    }
}