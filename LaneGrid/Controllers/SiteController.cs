﻿using LaneGrid.Handlers;
using LaneGrid.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LaneGrid.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SiteController : ApiController
    {
        private ISiteHandler handler;

        public SiteController(ISiteHandler handler) 
        {
            this.handler = handler;
        }

        [HttpGet]
        [Route("sites")]
        public Site[] GetAll()
        {
            return handler.GetSites();
        }

        [HttpGet]
        [Route("site/{siteid}")]
        public Site Get(string siteid)
        {
            return handler.GetSite(siteid);
        }

    }
}
