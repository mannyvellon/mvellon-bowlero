﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using LaneGrid.Models;
using LaneGrid.Handlers;
using System.Web.Http.Cors;
using System.CodeDom.Compiler;

namespace LaneGrid.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ResourceController : ApiController
    {
        private IResourceHandler handler;

        public ResourceController(IResourceHandler handler)
        {
            this.handler = handler;
        }

        [HttpGet]
        [Route("resource/get/{id}")]
        public Resource Get(string id)
        {
            return handler.GetResource(id);
        }

        [HttpGet]
        [Route("resources/center/{id}")]
        public ResourceTree[] GetCenterResources(string id)
        {
            return new ResourceTree[] { handler.GetCenterResources(id) };
        }



        // POST: api/resources/<center>
        [HttpPost]
        [Route("resources/center/{id}")]
        public void Post(string id, [FromBody] ResourceTree[] resourceTree)
        {
            if (resourceTree.Length != 1)
                throw new Exception("Centers must have a single head resource");
            handler.AddCenterResources(id, resourceTree[0]);
        }

        [HttpGet]
        [Route("resources/getcenterlanesresourceids/{ecId}")]
        public List<string> GetCenterLaneResourceIds(string ecId)
        {
            return handler.GetCenterLaneResourceIds(ecId);
        }

        [HttpGet]
        [Route("resources/lma/getcenterlanesresourceids/{siteid}")]
        public List<string> GetLmaCenterLaneResourceIds(string siteid)
        {
            return handler.GetLmaCenterLaneResourceIds(siteid);
        }

        [HttpGet]
        [Route("resources/getavailableslots/")]
        public IHttpActionResult GetAvailableSlots([FromUri]Guid productId, [FromUri] Guid ecId, [FromUri]string startDate, [FromUri]Guid eventTypeId, [FromUri] string selectedNoOfPeople, [FromUri] string HoursRequired)
        {
            var results = handler.GetAvailableSlots(productId, ecId, startDate, eventTypeId, selectedNoOfPeople, HoursRequired, false);
            if (results == null)
            {
                return NotFound();
            }
            return Ok(results);

        }

        [HttpGet]
        [Route("resources/getavailableslotsalacarte/")]
        public IHttpActionResult GetAvailableSlotsForAlaCarte([FromUri]Guid productId, [FromUri] Guid ecId, [FromUri]string startDate, [FromUri]Guid eventTypeId, [FromUri] string selectedNoOfPeople, [FromUri] string HoursRequired)
        {
            var results = handler.GetAvailableSlots(productId, ecId, startDate, eventTypeId, selectedNoOfPeople, HoursRequired, true);
            if (results == null)
            {
                return NotFound();
            }
            return Ok(results);

        }

        [HttpGet]
        [Route("resources/lma/getresourceavailability/{resourceId}/{startdate}/{enddate}")]
        public IHttpActionResult GetResourceAvailability(string resourceId, string startdate, string enddate)
        {
            var result = handler.GetResourceAvailability(resourceId, startdate, enddate);
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

    }
}
