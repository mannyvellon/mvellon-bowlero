﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;
using System.Web.Http;

using LaneGrid.Models;
using LaneGrid.Handlers;
using System.Web.Http.Cors;

namespace LaneGrid.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ActivityController : ApiController
    {
        private IActivityHandler handler;

        public ActivityController(IActivityHandler handler)
        {
            this.handler = handler;
        }

        [HttpGet]
        [Route("activity/get/{id}")]
        public Activity Get(string id)
        {
            return handler.Get(id);
        }

        [HttpPost]
        [Route("activity/create")]
        public IHttpActionResult Create([FromBody]Activity activity)
        {
            try
            {
                return Ok(handler.Create(activity));

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return null;
            }
        }
        [HttpGet]
        [Route("activity/getmultiple/{siteid}/{sdate}/{edate}/{userid}")]
        public Activity[] Get(string siteid, string sdate, string edate, string userid)
        {
            return handler.Get(siteid, sdate, edate, userid);
        }

        [HttpGet]
        [Route("activity/forevent/{eventid}")]
        public Activity[] GetForvent(string eventid)
        {
            return handler.GetForEvent(eventid);
        }

        // POST: api/Activity/<center>
        [HttpPost]
        [Route("activity/add/{siteid}")]
        public string Post(string siteid, [FromBody] Activity[] activities)
        {
            return handler.Add(siteid, activities).ToString();
        }

        [HttpPost]
        [Route("activity/update")]
        public void UpdateActivities([FromBody] Activity activities)
        {
            handler.Update(new Activity[] { activities });
        }


        [HttpPost]
        [Route("activity/updatemultiple")]
        public void UpdateActivities([FromBody] Activity[] activities)
        {
            handler.Update(activities);
        }

        [HttpPost]
        [Route("activity/cancel")]
        public void CancelActivity([FromBody] Activity a)
        {
            handler.Cancel(a);
        }

    }
}
