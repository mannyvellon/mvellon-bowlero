﻿using Antlr.Runtime.Tree;
using LaneGrid.Handlers;
using LaneGrid.Helpers;
using LaneGrid.Models;
using LaneGrid.Storage;
using LaneGrid.Common;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.Xrm.Sdk.Query;
using BAMF.MSCRM.BusinessEntities.Enums;

namespace LaneGrid.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class VampireController : ApiController
    {
        private IVampireHandler handler;
        private IStore store;

        public VampireController(IVampireHandler handler)
        {
            this.handler = handler;
        }

        [HttpGet]
        [Route("vampire/sites")]
        public int VampireSites()
        {
            return handler.VampireSites();
        }

        [HttpGet]
        [Route("vampire/activities/{siteid}/{startDate}/{endDate}")]
        public int VampireActivities(string siteid, string startDate, string endDate)
        {
            return handler.VampireActivities(siteid, startDate, endDate);
        }

        [HttpGet]
        [Route("vampire/resources/{siteid}")]
        public int VampireResources(string siteid)
        {
            return handler.VampireResources(siteid);
        }



    }
}
