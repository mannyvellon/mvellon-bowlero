﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using LaneGrid.Handlers;
using LaneGrid.Storage;

namespace LaneGrid
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            System.Web.Http.GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            config.Formatters.Insert(0, new System.Net.Http.Formatting.JsonMediaTypeFormatter());
            //enable cross domain access
            // add common seperated domains to support cross domain
            //var cors = new EnableCorsAttribute("http://dev-gemswf01.na.amfbowl.net,http://qa-gemswf01.na.amfbowl.net,http://uat-gemswf01.na.amfbowl.net,http://uat-gemsns01.na.amfbowl.net,http://uat-gemsns01,http://uat-gemswf01,http://dev-gemswf01,http://qa-gemswf01,http://localhost:61985,http://192.168.0.80,http://localhost:55540, http://localhost:8080, http://localhost,http://dev-olbweb01,http://qa-olbweb01,http://dmz-olbweb01u, http://10.152.9.10,http://localhost:62171,http://10.19.103.15,http://localhost:55540,https://uat-gemswf.na.amfbowl.net,https://uat-gemswf,http://uat-gemswf.na.amfbowl.net,http://uat-gemswf,http://172.99.115.250,http://104.130.137.146,https://gems,https://gems.bowlmor-amf.com,https://10.152.1.10,http://10.152.1.10,http://dotnetcoreangular.azurewebsites.net,http://localhost:55540", " *", "*");
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            
            // Web API configuration and services 
            UnityContainer container = new UnityContainer();
            container.RegisterType<IActivityHandler, ActivityHandler>(new HierarchicalLifetimeManager());
            container.RegisterType<IEventHandler, Handlers.EventHandler>(new HierarchicalLifetimeManager());
            container.RegisterType<IResourceHandler, ResourceHandler>(new HierarchicalLifetimeManager());
            container.RegisterType<ISiteHandler, SiteHandler>(new HierarchicalLifetimeManager());
            container.RegisterType<IVampireHandler, VampireHandler>(new HierarchicalLifetimeManager());
            container.RegisterType<ICache, CacheRedis>(new HierarchicalLifetimeManager());
            container.RegisterType<IStore, SqlStore>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new Unity.UnityResolver(container);


            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
