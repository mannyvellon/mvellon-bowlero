﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LaneGrid.Common
{
    public class Constants
    {
        public class FetchSql
        {
            public const string Activity = @"SELECT * FROM Activity WHERE id=@id";
            public const string Activities = @"SELECT * FROM Activity WHERE siteid=@siteid AND start>=@sdate and end<=@edate";
            public const string Resource = @"SELECT * FROM Resource WHERE id=@id";
            public const string CenterResources = @"SELECT * FROM CenterResources WHERE id=@id";
            public const string Sites = @"SELECT * FROM Sites";
            public const string Site = @"SELECT * FROM Sites WHERE id=@id";
            public const string Event = @"SELECT * FROM Events WHERE id=@id";
            public const string EventActivities = @"SELECT idActivity FROM EventActivities WHERE idEvent=@idEvent";
        }

        public class FetchXmls
        {
            public const string OpportunityRetrieveFetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                          <entity name='opportunity'>
                                                            <attribute name='name' />
                                                            <attribute name='customerid' />
                                                            <attribute name='estimatedvalue' />
                                                            <attribute name='statuscode' />
                                                            <attribute name='statecode' />
                                                            <attribute name='opportunityid' />
                                                            <attribute name='bamf_totaldue' />
                                                            <attribute name='bamf_expectednumberinparty' />
                                                            <attribute name='bamf_eventstart' />
                                                            <attribute name='bamf_eventend' />
                                                            <attribute name='bamf_entertainmentcenterid' />
                                                            <attribute name='bamf_promotionid' />
                                                            <attribute name='bamf_expectednumberofchildren' />
                                                            <attribute name='bamf_expectednumberofadults' />
                                                            <order attribute='name' descending='false' />
                                                            <filter type='and'>
                                                              <condition attribute='opportunityid' operator='eq' uitype='opportunity' value='{0}' />
                                                            </filter>
                                                          </entity>
                                                        </fetch>";
            public const string ProductResourceAlaCarteFetch = @"<fetch distinct='true' mapping='logical' output-format='xml-platform' version='1.0' >
                                                            <entity name='bamf_productresource' >
                                                                <attribute name='bamf_resourcetype' />
                                                                <attribute name='bamf_maxcapacity' />
                                                                <order descending='true' attribute='bamf_productid' />
                                                                <filter type='and' >
                                                                    <condition attribute='bamf_entertainmentcenterid' value='{1}' uitype='bamf_entertainmentcenteradministration' operator='eq' />
                                                                </filter>
                                                                <link-entity name='product' alias='ae' to='bamf_productid' from='productid' >
                                                                               <filter type='and' >
                                                                                <condition attribute='productid' value='{0}' uitype='product' operator='eq' />
                                                                            </filter>
                                                                   
                                                                </link-entity>
                                                            </entity>
                                                        </fetch>";
            public const string ProductResourceFetch = @"<fetch distinct='true' mapping='logical' output-format='xml-platform' version='1.0' >
                                                            <entity name='bamf_productresource' >
                                                                <attribute name='bamf_resourcetype' />
                                                                <attribute name='bamf_maxcapacity' />
                                                                <order descending='true' attribute='bamf_productid' />
                                                                <filter type='and' >
                                                                    <condition attribute='bamf_entertainmentcenterid' value='{1}' uitype='bamf_entertainmentcenteradministration' operator='eq' />
                                                                </filter>
                                                                <link-entity name='product' alias='ae' to='bamf_productid' from='productid' >
                                                                    <link-entity name='bamf_bamf_pricingtierpackageoption_product' to='productid' from='productid' intersect='true' visible='false' >
                                                                        <link-entity name='bamf_pricingtierpackageoption' alias='af' to='bamf_pricingtierpackageoptionid' from='bamf_pricingtierpackageoptionid' >
                                                                            <filter type='and' >
                                                                                <condition attribute='bamf_packageid' value='{0}' uitype='product' operator='eq' />
                                                                            </filter>
                                                                        </link-entity>
                                                                    </link-entity>
                                                                </link-entity>
                                                            </entity>
                                                        </fetch>";
            public const string CentreHoursFetch = @"<fetch distinct='false' mapping='logical' output-format='xml-platform' version='1.0' >
                                                    <entity name='bamf_dailyschedule' >
                                                        <attribute name='bamf_dailyscheduleid' />
                                                        <attribute name='bamf_name' />
                                                        <attribute name='bamf_istiertime' />
                                                        <attribute name='bamf_starttimeminute' />
                                                        <attribute name='bamf_starttimehour' />
                                                        <attribute name='bamf_hoursscheduleid' />
                                                        <attribute name='bamf_endtimeminute' />
                                                        <attribute name='bamf_endtimehour' />
                                                        <attribute name='bamf_dayofweek' />
                                                        <order descending='false' attribute='bamf_name' />
                                                        <link-entity name='bamf_hourschedule' alias='hs' to='bamf_hoursscheduleid' from='bamf_hourscheduleid' >
                                                            <attribute name='bamf_scheduletype' />
                                                            <attribute name='bamf_eventtypeid' />
                                                            <attribute name='bamf_startdate' />
                                                            <attribute name='bamf_enddate' />
                                                            <filter type='and' >
                                                                <condition attribute='bamf_centerid' value='{0}'  operator='eq' />
                                                                <filter type='or' >
                                                                    <condition attribute='bamf_eventtypeid' value='{1}' uitype='bamf_eventtype' operator='eq' />
                                                                    <condition attribute='bamf_scheduletype' operator='in' >
                                                                        <value>
                                                                            100000001
                                                                        </value>
                                                                        <value>
                                                                            100000000
                                                                        </value>
                                                                    </condition>
                                                                </filter>
                                                            </filter>
                                                        </link-entity>
                                                    </entity>
                                                </fetch>";
            public const string ResourceDetailsOLB = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                      <entity name='equipment'>
                                                        <attribute name='name' />
                                                        <attribute name='equipmentid' />
                                                        <attribute name='siteid' />
                                                        <attribute name='businessunitid' />
                                                        <attribute name='bamf_room' />
                                                        <attribute name='bamf_resourcetype' />
                                                        <attribute name='bamf_order' />
                                                        <attribute name='bamf_floor' />
                                                        <order attribute='name' descending='false' />
                                                        <filter type='and'>
                                                          <condition attribute='siteid' operator='eq' uitype='siteid' value='{0}' />  
                                                        <condition attribute='bamf_resourcetype' operator='eq'  value='{1}' />                                                        
                                                        </filter>
                                                        <link-entity name='equipment' from='equipmentid' to='bamf_floor' visible='false' link-type='outer' alias='floor'>
                                                          <attribute name='bamf_room' />
                                                          <attribute name='bamf_resourcetype' />
                                                          <attribute name='bamf_order' />
                                                          <attribute name='bamf_floor' />
                                                         </link-entity>       
                                                        <link-entity name='equipment' from='equipmentid' to='bamf_room' visible='false' link-type='outer' alias='room'>
                                                          <attribute name='bamf_room' />
                                                          <attribute name='bamf_resourcetype' />
                                                          <attribute name='bamf_order' /> 
                                                                <link-entity name='equipment' from='equipmentid' to='bamf_floor' visible='false' link-type='outer' alias='roomParent'>
                                                                  <attribute name='bamf_room' />
                                                                    <attribute name='bamf_resourcetype' />
                                                                    <attribute name='bamf_order' />
                                                                    <attribute name='bamf_floor' />
                                                                </link-entity>                                                          
                                                        </link-entity>        
                                                      </entity>
                                                    </fetch>";
            public const string ServiceActivityRetrieveMultipleFetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false' count='{3}' paging-cookie='{4}' page='{5}'>
                                                                  <entity name='serviceappointment'>
                                                                    <attribute name='subject' />
                                                                    <attribute name='statecode' />
                                                                    <attribute name='bamf_scheduledstart' />
                                                                    <attribute name='bamf_scheduledend' />
                                                                    <attribute name='createdby' />
                                                                      <attribute name='createdon' />
                                                                    <attribute name='bamf_isbufferactivity' />                                                                            
                                                                    <attribute name='bamf_isretailhold' />               
                                                                     <attribute name='bamf_iseventhold' />                                                                              
                                                                    <attribute name='regardingobjectid' />
                                                                    <attribute name='activityid' />
                                                                    <attribute name='actualstart' />
                                                                    <attribute name='actualend' />
                                                                    <attribute name='activitytypecode' />
                                                                    <attribute name='siteid' />
                                                                    <attribute name='serviceid' />
                                                                    <attribute name='resources' />
                                                                    <attribute name='bamf_leaguetype' />
                                                                    <attribute name='bamf_leaguename' />
                                                                    <attribute name='bamf_leagueid' /><attribute name='bamf_guestcount' /><attribute name='bamf_totalrevenue' />
                                                                    <attribute name='bamf_clientid' />      
                                                                    <attribute name='customers' />                                                             
                                                                    <link-entity name='opportunity' from='opportunityid' to='regardingobjectid' visible='false' link-type='outer' alias='event'>
                                                                            <attribute name='bamf_partyname' />
                                                                            <attribute name='statuscode' />
                                                                            <attribute name='statecode' />
                                                                            <attribute name='bamf_totalamountofcontract' />
                                                                            <attribute name='ownerid' />
                                                                            <attribute name='bamf_expectednumberinparty' />
                                                                          <attribute name='bamf_eventstart' />
                                                                         <attribute name='bamf_eventend' />
                                                                   </link-entity>
                                                                   <link-entity name='site' from='siteid' to='siteid' alias='ae'>
                                                                      <link-entity name='bamf_entertainmentcenteradministration' from='bamf_entertainmentcenterid' to='siteid' alias='af'>
                                                                        <filter type='and'>
                                                                          <condition attribute='bamf_entertainmentcenteradministrationid' operator='eq'  uitype='bamf_entertainmentcenteradministration' value='{0}' />
                                                                        </filter>
                                                                      </link-entity>
                                                                    </link-entity>
                                                                    <order attribute='subject' descending='false' />
                                                                    <filter type = 'and' >
                                                                        <filter type='and'>
                                                                            <condition attribute = 'bamf_scheduledstart' operator='on-or-after' value='{1}' />
                                                                            <condition attribute = 'bamf_scheduledend' operator='on-or-before'  value='{2}' />
                                                                        </filter>
                                                                        <condition attribute='statecode' operator='in'>
                                                                                <value>0</value>
                                                                                <value>3</value>
                                                                                <value>1</value>
                                                                              </condition>
                                                                        </filter>
                                                                  </entity>
                                                                </fetch>";
            public const string ResourceRetrieveFetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                          <entity name='equipment'>
                                                            <attribute name='name'/>
                                                            <attribute name='equipmentid'/>
                                                            <attribute name='skills'/>
                                                            <attribute name='siteid'/>
                                                            <attribute name='bamf_room' />
                                                            <attribute name='bamf_order' />
                                                            <attribute name='bamf_floor' />
                                                            <attribute name='emailaddress'/>
                                                            <attribute name='description'/>
                                                            <attribute name='bamf_resourcetype' />
                                                            <order attribute='bamf_order' descending='false'/>
                                                            <link-entity name='site' from='siteid' to='siteid' alias='ae'>
                                                                    <link-entity name='bamf_entertainmentcenteradministration' from='bamf_entertainmentcenterid' to='siteid' alias='af'>
                                                                            <filter type='and'>
                                                                                <condition attribute='bamf_entertainmentcenteradministrationid' operator='eq'  uitype='bamf_entertainmentcenteradministration' value='{0}' />
                                                                            </filter>
                                                                    </link-entity>
                                                            </link-entity>
                                                            <link-entity name='equipment' from='equipmentid' to='bamf_floor' visible='false' link-type='outer' alias='floor'>
                                                                <attribute name='bamf_resourcetype' />
                                                             </link-entity>
                                                             <link-entity name='equipment' from='equipmentid' to='bamf_room' visible='false' link-type='outer' alias='room'>
                                                                <attribute name='bamf_resourcetype' />
                                                            	<attribute name='bamf_floor' />
                                                                <attribute name='bamf_order' />
                                                              </link-entity>
                                                          </entity>
                                                        </fetch>";

        }

        public class InsertSql
        {
            public const string Activity = @"REPLACE INTO Activity(
                                id, 
                                start, 
                                end, 
                                text, 
                                resource, 
                                cssClass,
                                resizeDisabled,
                                siteid,
                                backColor,
                                oppoid,
                                repname,
                                guestcount,
                                onholddate,
                                totalrevenue,
                                userid,
                                partyname,
                                isretailhold,
                                iseventhold,
                                createdbyname,
                                isbufferactivity,
                                isleague,
                                leagueid,
                                leaguename,
                                leaguetype,
                                subject,
                                oppoStart,
                                oppoEnd)
                            VALUES(
                                @id, 
                                @start, 
                                @end, 
                                @text, 
                                @resource, 
                                @cssClass,
                                @resizeDisabled,
                                @siteid,
                                @backColor,
                                @oppoid,
                                @repname,
                                @guestcount,
                                @onholddate,
                                @totalrevenue,
                                @userid,
                                @partyname,
                                @isretailhold,
                                @iseventhold,
                                @createdbyname,
                                @isbufferactivity,
                                @isleague,
                                @leagueid,
                                @leaguename,
                                @leaguetype,
                                @subject,
                                @oppoStart,
                                @oppoEnd)";

            public const string Resource = @"REPLACE INTO Resource(
                                id,
                                name,
                                expanded,
                                type,
                                parentid)
                            VALUES(
                                @id,
                                @name,
                                @expanded,
                                @type,
                                @parentid)";

            public const string CenterResources = @"REPLACE INTO CenterResources(id, json) VALUES(@id,@json)";
            public const string Event = @"REPLACE INTO Events(id, centerid) VALUES(@id, @centerid)";
            public const string EventActivities = @"REPLACE INTO EventActivities(idEvent, idActivity) VALUES(@idEvent, @idActivity)";
            public const string Sites = @"REPLACE INTO Sites(id, entertainmentCenterId, name, siteNumber) 
                                                       VALUES(@id, @entertainmentCenterId, @name, @siteNumber)";
        }

        public class UpdateSql
        {
            public const string Activity = @"UPDATE Activity SET
                                                start=@start,
                                                end=@end,
                                                resource=@resource
                                             WHERE id=@id";

        }

        public class DeleteSql
        {
            public const string Activity = @"DELETE FROM Activity WHERE id=@id";
        }
        
        public class Attributes
        {
            public class Resource
            {
                public const string Name = "name";
                public const string ResourceType = "bamf_resourcetype";
                public const string Order = "bamf_order";
                public const string Floor = "bamf_floor";
                public const string Room = "bamf_room";
                public const string SiteId = "siteid";
                public const string AliasedFloor = "room.bamf_floor";
                public const string AliasedOrder = "room.bamf_order";
                public const string RoomResourceType = "room.bamf_resourcetype";
                public const string FloorResourceType = "floor.bamf_resourcetype";
                public const string ResourceId = "bamf_entertainmentresourceid";
            }

            public class ProductResource
            {
                public const string ResourceType = "bamf_resourcetype";

                public const string MaxCapacity = "bamf_maxcapacity";
            }

            public class Site
            {
                public const string Name = "bamf_name";
                public const string EntCenter = "bamf_entertainmentcenterid";
                public const string EntCenterAdminId = "bamf_entertainmentcenteradministrationid";
                public const string SiteNumber = "bamf_sitenumber";
                public const string IsGemCenter = "bamf_isgemscenter";
                public const string EntityName = "site";
                public const string PrimaryKey = "siteid";
                public const string TimeZoneCode = "timezonecode";
                public const string PaymentProcessorPassword = "bamf_paymentprocessorpassword";
                public const string EventFee = "bamf_eventfeepercentage";
                public const string BrandId = "bamf_brandid";
                public const string DistrictId = "bamf_districtid";
                public const string CenterNumber = "bamf_prefix";
                public const string Status = "statuscode";
                public const string StreetAddress = "bamf_streetaddress";
                public const string City = "bamf_city";
                public const string State = "bamf_stateprovinceid";
                public const string Zip = "bamf_postalzipcode";
                public const string PhoneNumber = "bamf_phonenumber";
                public const string BookingPhoneNo = "bamf_bookingphonenumber";
                public const string Fax = "bamf_fax";
                public const string Latitude = "bamf_latitude";
                public const string Longitude = "bamf_longitude";
                public const string GMemail = "bamf_generalmanageremail";
                public const string CenterEmail = "bamf_centeremail";
                public const string BookingEmail = "brand.bamf_bookingemail";
                public const string StateIsoCode = "state.bamf_isocode";
                public const string IsRFPOnly = "bamf_isrfponly";
                public const string IsExperential = "bamf_experential";

            }

            public class HoursSchedule
            {
                public const string Name = "hs.bamf_name";
                public const string Id = "hs.bamf_hoursscheduleid";
                public const string ScheduleType = "hs.bamf_scheduletype";
                public const string EventTypeId = "hs.bamf_eventtypeid";
                public const string StartDate = "hs.bamf_startdate";
                public const string EndDate = "hs.bamf_enddate";
            }

            public class DailySchedule
            {
                public const string Name = "bamf_name";
                public const string IsTierTime = "bamf_istiertime";
                public const string StartMinutes = "bamf_starttimeminute";
                public const string StartHours = "bamf_starttimehour";
                public const string Id = "bamf_dailyscheduleid";
                public const string EndMinutes = "bamf_endtimeminute";
                public const string EndHours = "bamf_endtimehour";
                public const string DayOfWeek = "bamf_dayofweek";
            }

            public class Event
            {
                public const string Name = "name";
                public const string OpportunityId = "opportunityid";
                public const string IsOverBooked = "bamf_isoverbooked";
                public const string BookedBy = "bamf_bookedbyid";
                public const string ExpectedNumInParty = "bamf_expectednumberinparty";
                public const string StatusCode = "statuscode";
                public const string StateCode = "statecode";
                public const string EventStart = "bamf_eventstart";
                public const string EventEnd = "bamf_eventend";
                public const string Promotion = "bamf_promotionid";
                public const string EventType = "bamf_eventtypeid";
                public const string SubEventType = "bamf_subeventtypeid";
                public const string Owner = "ownerid";
                public const string EcId = "bamf_entertainmentcenterid";
                public const string DateBooked = "bamf_datebooked";
                public const string Source = "bamf_sourceid";
                public const string SourceDetails = "bamf_sourcedetails";
                public const string IsOnline = "bamf_isonlinebooking";
                public const string Contact = "parentcontactid";
                public const string PotentialCustomer = "customerid";
                public const string ParentContactId = "parentcontactid";
                public const string EventID = "name";
                public const string NoOfAdult = "bamf_expectednumberofadults";
                public const string NoOfChildren = "bamf_expectednumberofchildren";
                public const string NoInParty = "bamf_expectednumberinparty";
                public const string NoOfPeople = "bamf_numberofpeople";
                public const string NoOfUnderOver21 = "bamf_alacartenumberofpeople";
                public const string HowDidYouHearAboutUs = "bamf_howdidyouhearaboutus";
                public const string PartyName = "bamf_partyname";
                public const string BirthdayChildName = "bamf_birthdaychildname";
                public const string TotalDue = "bamf_totaldue";
                public const string ParentAccountId = "parentaccountid";
                public const string FBNotBalanced = "bamf_fbnotbalanced";
                public const string SiteId = "site.bamf_entertainmentcenterid";
                public const string Account = "parentaccountid";
                public const string BudgetAmount = "budgetamount";
                public const string CompanyName = "bamf_companyname";
                public const string OfferCode = "bamf_offercode";
            }

            public class Activity
            {
                public const string ActivityId = "activityid";
                public const string Subject = "subject";
                public const string Resources = "resources";
                public const string RegardingObjectId = "regardingobjectid";
                public const string CRMScheduledStart = "scheduledstart";
                public const string CRMScheduledEnd = "scheduledend";

                public const string ScheduledStart = "bamf_scheduledstart";
                public const string ScheduledEnd = "bamf_scheduledend";
                public const string IsRetailHold = "bamf_isretailhold";
                public const string IsEventHold = "bamf_iseventhold";
                public const string ServiceId = "serviceid";
                public const string SiteId = "siteid";
                public const string StatusCode = "statuscode";
                public const string StateCode = "statecode";
                public const string BusinessLastActivityDate = "event.bamf_businessdayslastactivity";
                public const string EventExpctedNumber = "event.bamf_expectednumberinparty";
                public const string EventBookedBy = "event.bamf_bookedbyid";
                public const string EventPartyName = "event.bamf_partyname";
                public const string EventActiveQuote = "event.bamf_activequoteid";
                public const string ClientID = "bamf_clientid";
                public const string LeagueID = "bamf_leagueid";
                public const string LeagueName = "bamf_leaguename";
                public const string LeagueType = "bamf_leaguetype";
                public const string GuestCount = "bamf_guestcount";
                public const string TotalRevenueFromActivity = "bamf_totalrevenue";
                public const string IsImpactedByLaneGrid = "bamf_isimpacted";
                public const string OpportunityStatus = "event.statuscode";
                public const string TimeZoneCode = "timezoneruleversionnumber";
                public const string UTCConversionCode = "utcconversiontimezonecode";
                public const string TotalRevenue = "event.bamf_totalamountofcontract";
                public const string RepName = "event.ownerid";
                public const string OppStart = "event.bamf_eventstart";
                public const string OppEnd = "event.bamf_eventend";
                public const string CreatedBy = "createdby";
                public const string CreatedOn = "createdon";
                public const string Customers = "customers";
                public const string IsBufferActivity = "bamf_isbufferactivity";
            }

            public class ActivityParty
            {
                public const string PartyId = "partyid";
                public const string PartyObjectTypeCode = "partyobjecttypecode";
            }

        }

        public class EntityName
        {
            public const string Opportunity = "opportunity";
            public const string ErrorLog = "bamf_errorlog";
            public const string Equipment = "equipment";
            public const string ActivityParty = "activityparty";
            public const string DiscountApproval = "bamf_discountapproval";
            public const string ServiceAppointment = "serviceappointment";
            public const string Service = "service";
            public const string Site = "site";
            public const string Contract = "quote";
            public const string ContractLine = "quotedetail";
            public const string ContractLineItem = "bamf_contractlineitem";
            public const string EntCenterAdmin = "bamf_entertainmentcenteradministration";
            public const string Product = "product";
            public const string unit = "uom";
            public const string ProductCategory = "bamf_productcategory";
            public const string PackageOption = "bamf_pricingtierpackageoption";
            public const string EventType = "bamf_eventtype";
            public const string Contact = "contact";
            public const string Source = "bamf_source";
            public const string Promotion = "bamf_promotion";
            public const string HearAboutUs = "bamf_howdidyouhearaboutus";
            public const string States = "bamf_stateprovinces";
            public const string SystemUser = "systemuser";
            public const string Account = "account";
        }

        public class ResourceProduct
        {
            public const string EntityName = "bamf_productresource";
            public const string Product = "bamf_productid";
            public const string MaxCapacity = "bamf_maxcapacity";
            public const string EntertainmentCenterId = "ec.bamf_entertainmentcenterid";
            public const string EntertainmentCenter = "bamf_entertainmentcenterid";
            public const string ResourceType = "bamf_resourcetype";
            public const string SessionDuration = "ao.bamf_duration";
            public const string ProductName = "ao.name";
            public const string ProductType = "ap.producttypecode";
            public const string ProductUnit = "ap.uomid";
            public const string Quantity = "ap.quantity";
            public const string Extension = "ap.bamf_extension";
            public const string Duration = "ap.bamf_duration";
            public const string CustomQuantity = "ap.bamf_resourcequantity";
        }

        public class Resource
        {
            public const string EntityName = "equipment";
            public const string Name = "name";
            public const string ResourceType = "bamf_resourcetype";
            public const string Order = "bamf_order";
            public const string Floor = "bamf_floor";
            public const string Room = "bamf_room";
        }



    }
}