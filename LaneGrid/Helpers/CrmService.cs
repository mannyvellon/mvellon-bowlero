﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LaneGrid.Helpers
{
    public static class CrmService
    {
        static string crmConnectionString = ConfigurationManager.AppSettings["crmConnectionString"];
        private static CrmServiceClient _crmServiceClient = null;
        private static CrmServiceClient _crmServiceUser = null;

        internal static IOrganizationService GetInstance()
        {
            // get a client if none cached
            if (_crmServiceClient == null || !_crmServiceClient.IsReady)
            {
                _crmServiceClient = new CrmServiceClient(crmConnectionString);
            }

            // if have a client and it's ready, set a timeout and return it
            if (_crmServiceClient != null && _crmServiceClient.IsReady)
            {
                // does this have to be done every time?
                _crmServiceClient.OrganizationServiceProxy.Timeout = new TimeSpan(1, 60, 00);
                return (IOrganizationService)_crmServiceClient.OrganizationServiceProxy;
            }

            // else, can't do anything
            return null;
        }

        internal static CrmServiceClient GetClientInstance()
        {
            // if we don't have a cached or ready one, create a new one
            if (_crmServiceClient == null || !_crmServiceClient.IsReady)
            {
                _crmServiceClient = new CrmServiceClient(crmConnectionString);
            }

            // if we have a cached one and it's ready, return it
            if (_crmServiceClient != null && _crmServiceClient.IsReady)
            {
                return _crmServiceClient;
            }

            // no cache and couldn't create. Fail
            throw new Exception("Unable to connect to Crm server");
        }
        internal static OrganizationServiceProxy GetInstance(Guid userId)
        {
            // if no cache or not ready, create a new one
            if (_crmServiceUser == null || !_crmServiceUser.IsReady)
            {
                _crmServiceUser = new CrmServiceClient(crmConnectionString);
            }

            // if cached and ready, return it
            if (_crmServiceUser != null && _crmServiceUser.IsReady)
            {
                _crmServiceUser.OrganizationServiceProxy.CallerId = userId;
                _crmServiceUser.OrganizationServiceProxy.Timeout = new TimeSpan(1, 60, 00);
                return _crmServiceUser.OrganizationServiceProxy;
            }

            // no cache, couldn't create. Fail
            return null;
        }
    }
}
