﻿using Google.Protobuf.WellKnownTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaneGrid.Storage
{
    public interface ICache
    {
        bool Exists(string id);
        bool ExistsInMap(string idMap, string idEntry);

        void Store(string id, string value, int timeoutSecs=int.MaxValue);
        void StoreInMap(string idMap, string idEntry, string value);
        string Get(string id);
        string GetFromMap(string idMap, string idEntry);
        string[] GetMapValues(string idMap);

        string[] GetKeys(string pattern);
        string[] GetMapKeys(string idMap);

        void Remove(string id);
        void RemoveFromMap(string idMap, string identry);
    }
}
