﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using StackExchange.Redis;

namespace LaneGrid.Storage
{
    public class CacheRedis : ICache
    {
        private string redisConnectionString;
        private ConnectionMultiplexer redisMultiplexer;
        private IDatabase redis;

        public CacheRedis()
        {
            redisConnectionString = ConfigurationManager.AppSettings.Get("redisConnectionString");
            try
            {
                redisMultiplexer = ConnectionMultiplexer.Connect(redisConnectionString);
                redis = redisMultiplexer.GetDatabase();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public bool Exists(string id)
        {
            return redis.KeyExists(id);
        }

        public bool ExistsInMap(string idMap, string idEntry)
        {
            return redis.HashExists(idMap, idEntry);
        }

        public string Get(string id)
        {
            return redis.StringGet(id);
        }

        public string GetFromMap(string idMap, string idEntry)
        {
            return redis.HashGet(idMap, idEntry);
        }

        public string[] GetMapValues(string idMap)
        {
            return redis.HashValues(idMap).ToStringArray();
        }

        public void Store(string id, string value, int timeoutSecs=int.MaxValue)
        {
            if (timeoutSecs == int.MaxValue)
                // write to cache
                redis.StringSet(id, value);
            else
                redis.StringSet(id, value, TimeSpan.FromSeconds(timeoutSecs));
        }

        public void StoreInMap(string idMap, string idEntry, string value)
        {
            redis.HashSet(idMap, idEntry, value);
        }

        public string[] GetKeys(string pattern)
        {
            List<string> keys = new List<string>();

            foreach (EndPoint ep in redisMultiplexer.GetEndPoints())
            {
                IServer server = redisMultiplexer.GetServer(ep);
                foreach (RedisKey key in server.Keys(0, pattern))
                {
                    keys.Add(key);
                }
            }

            return keys.ToArray();
        }

        public string[] GetMapKeys(string idMap)
        {
            return redis.HashKeys(idMap).ToStringArray();
        }

        public void Remove(string key)
        {
            redis.KeyDelete(key);
        }

        public void RemoveFromMap(string idMap, string identry)
        {
            redis.HashDelete(idMap, identry);
        }

    }
}