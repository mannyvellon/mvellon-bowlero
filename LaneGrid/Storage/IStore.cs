﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LaneGrid.Models;
using Renci.SshNet.Security;

namespace LaneGrid.Storage
{
    public interface IStore
    {

        // cache functions
        void StoreInCache(string key, string value, int timeoutSecs=int.MaxValue);
        bool IsCached(string key);
        string GetFromCache(string key);
        
        int StoreActivities(Activity[] activties, string siteid);
        void UpdateActivities(Activity[] activities);

        Activity GetActivity(string id);

        Activity[] GetActivities(string siteid, string sdate, string edate, string userid);
        Activity[] GetActivitiesForEvent(string idEvent);

        void Cancel(Activity a);

        void StoreCenterResources(string id, ResourceTree resourceTree);
        ResourceTree GetCenterResources(string id);
        Resource GetResource(string id);

        Event GetEvent(string id, bool withActivities);

        Site[] GetSites();
        Site GetSite(string siteid);
        Site GetSiteByEcId(string ecId);
        void StoreSites(Site[] s);

        string[] GetKeys(string pattern);
        List<string> GetCenterResourceIds(string siteid);
        List<string> GetLmaCenterLaneResourceIds(string siteid);
    }
}