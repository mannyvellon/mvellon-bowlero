﻿using LaneGrid.Models;
using System;
using Newtonsoft.Json;

using System.Collections.Generic;
using System.Web;

using MySql.Data.MySqlClient;
using System.Configuration;
using System.Reflection;
using System.Threading;
using LaneGrid.Common;
using Microsoft.Ajax.Utilities;
using BAMF.MSCRM.BusinessEntities.Enums;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace LaneGrid.Storage
{
    public class SqlStore : IStore
    {
        private ICache cache;
        public static string mysqlConnectionString;

        public SqlStore(ICache cache)
        {
            this.cache = cache;
            SqlStore.mysqlConnectionString = ConfigurationManager.AppSettings.Get("mysqlConnectionString");

        }

        #region cachestuff
        public bool IsCached(string id)
        {
            return cache.Exists(id);
        }

        public bool IsInCachedMap(string idMap, string idEntry)
        {
            return cache.ExistsInMap(idMap, idEntry);
        }

        public void StoreInCache(string key, string value, int timeoutSecs=int.MaxValue)
        {
            cache.Store(key, value, timeoutSecs);
        }

        public string GetFromCache(string key)
        {
            return cache.Get(key);
        }

        public string[] GetKeys(string pattern)
        {
            // TODO: do with database, not with cache!
            return cache.GetKeys(pattern);
        }

        /*
         * Functions to format keys for cache
         */

        private string SiteDayKey(string siteid, string date)
        {
            return string.Format("siteday-{0}-{1}", siteid, date);
        }

        private string SiteDayKey(Activity a)
        {
            return SiteDayKey(a.id, a.start);
        }

        private string ActivityKey(string id)
        {
            return string.Format("activity-{0}", id);
        }

        private string CenterResourcesKey(string id)
        {
            return string.Format("resources-{0}", id);
        }

        private string ResourceKey(string id)
        {
            return string.Format("resource-{0}", id);
        }
        
        private string EventKey(string id)
        {
            return string.Format("event-{0}", id);
        }

        private string SitesKey()
        {
            return "sites";
        }

        private string SiteKey(string siteid)
        {
            return string.Format("site-{0}", siteid);
        }

        #endregion

        #region Activity

        /*
         * This function only works against the database since we don't have enough information to find it in the cache
         */
        public Activity GetActivity(string id)
        {
            // check cache
            string json = cache.Get(ActivityKey(id));
            if (json != null)
                return JsonConvert.DeserializeObject<Activity>(json);

            // get from database
            MySqlDataReader rdr = MySqlHelper.ExecuteReader(mysqlConnectionString,
                                                            Constants.FetchSql.Activity,
                                                            new MySqlParameter[] { new MySqlParameter("@id", id) });
            if (rdr.Read())
            {
                Activity a = new Activity();

                PropertyInfo[] pis = typeof(Activity).GetProperties();
                foreach (PropertyInfo pi in pis)
                {
                    object value = rdr[pi.Name];
                    if (!DBNull.Value.Equals(value))
                    {
                        // treat booleans special
                        if (pi.PropertyType.Name == "Boolean")
                            pi.SetValue(a, rdr[pi.Name].ToString() != "0");
                        else
                            pi.SetValue(a, rdr[pi.Name]);
                    }

                }

                // put in cache
                string s = JsonConvert.SerializeObject(a);
                cache.Store(ActivityKey(a.id), s);

                return a;

            }
            return null;
        }

        /*
         * This function is really only called when initially migrating data from Dynamics into 
         * the Lane Grid. Don't bother with caching.
         */
        public int StoreActivities(Activity[] activities, string siteid)
        {
            Dictionary<string, Event> dic = new Dictionary<string, Event>();
            foreach (Activity a in activities)
            {
                // poke the site into the object
                a.siteid = siteid;

                // if we don't already have an event in the dictionary, create it
                if (a.oppoid != null)
                {
                    Event ev;
                    if (dic.ContainsKey(a.oppoid))
                    {
                        ev = dic[a.oppoid];
                    }
                    else
                    {
                        ev = new Event();
                        ev.id = a.oppoid;
                        ev.centerid = siteid;
                        ev.activities = new List<Activity>();
                        dic.Add(a.oppoid, ev);
                    }

                    // add it to the event
                    ev.activities.Add(a);

                   
                }

                // TODO: maybe invalidate caches

                // store it

                // Lazy write to database
                ThreadPool.QueueUserWorkItem(LazyWriter.Write, a);

                // create extra entries, if necessary
                DateTime dtStart = DateTime.Parse(a.start + "Z");
                DateTime dtEnd = DateTime.Parse(a.end + "Z");
                if (dtEnd.DayOfYear != dtStart.DayOfYear && dtEnd.Hour > 0)
                {
                    Console.Error.WriteLine("Activity spanning over multiple days! ");
                }
            }

            // Now, write out the event information
            foreach (string key in dic.Keys)
            {
                // write out the event
                Event ev = dic[key];
                MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString,
                                                Constants.InsertSql.Event,
                                                new MySqlParameter[] { new MySqlParameter("@id", ev.id),
                                                                       new MySqlParameter("@centerid", siteid) });

                // now write out the activities in the event
                foreach (Activity a in ev.activities)
                {
                    List<MySqlParameter> p = new List<MySqlParameter>();
                    p.Add(new MySqlParameter("@idEvent", ev.id));
                    p.Add(new MySqlParameter("@idActivity", a.id));
                    MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString, Constants.InsertSql.EventActivities, p.ToArray());
                }

            }

            return activities.Length;
        }

        public Activity[] GetActivitiesForEvent(string idEvent)
        {
            Event ev = GetEvent(idEvent, false);

            if (ev == null)
                throw new Exception("Invalid event id");

            // if we have activities, return them
            if (ev.activities.Count>0)
                return ev.activities.ToArray();

            // else, go look for them 

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@idEvent", idEvent));
            MySqlDataReader rdr = MySqlHelper.ExecuteReader(SqlStore.mysqlConnectionString,
                                                             Constants.FetchSql.EventActivities,
                                                             parms.ToArray());
            List<Activity> activities = new List<Activity>();
            while (rdr.Read())
            {
                string idActivity = rdr["idActivity"] as string;
                Activity a = GetActivity(idActivity);
                if (a != null)
                    activities.Add(a);
            }

            // add the activities to the event and store in the cache
            ev.activities = activities;
            string json = JsonConvert.SerializeObject(ev);
            cache.Store(EventKey(idEvent), json);

            return activities.ToArray();
        }

        public void UpdateActivities(Activity[] activities)
        {
            foreach (Activity a in activities)
            {
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("@start", a.start));
                parms.Add(new MySqlParameter("@end", a.end));
                parms.Add(new MySqlParameter("@id", a.id));
                parms.Add(new MySqlParameter("@resource", a.resource));
                MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString, Constants.UpdateSql.Activity, parms.ToArray());

                // update cache
                string json = JsonConvert.SerializeObject(a);
                cache.Store(ActivityKey(a.id), json);
                cache.StoreInMap(SiteDayKey(a), a.id, json);
            }
        }

        /*
         * This is the most important function to get right. We advance day by day looking to see if things are
         * cached. That lets overlapping requests benefit from each other
         */
        public Activity[] GetActivities(string siteid, string sdate, string edate, string userid)
        {
            // first, get the site
            Site s = GetSite(siteid);
            if (s == null)
                throw new Exception("Invalid site: " + siteid);

            // now, look for activities at the given site, between the given dates

            // first, convert dates to a useful form
            DateTime sdt = DateTime.Parse(sdate);
            sdate = sdt.ToString("yyyy-MM-dd") + "T00:00:00";
            DateTime edt = DateTime.Parse(edate);
            edate = edt.ToString("yyyy-MM-dd") + "T23:59:59";

            // Go day by day
            List<Activity> activities = new List<Activity>();
            TimeSpan ts = edt - sdt;
            for (int day=0; day<ts.TotalDays; day++)
            {
                // get day's activities
                DateTime dt = sdt + TimeSpan.FromDays(day);
                activities.AddRange(GetDaysActivitiesForSite(siteid, dt));
            }

            // all done
            return activities.ToArray();

        }

        private List<Activity> GetDaysActivitiesForSite(string siteid, DateTime dt)
        {
            List<Activity> activities;

            // check cache
            string date = dt.ToString("yyyy-MM-dd");
            string siteDayKey = SiteDayKey(siteid, date);

            if (IsCached(siteDayKey))
            {
                // get the activities (in JSON form)
                string[] jsonActivities = cache.GetMapValues(siteDayKey);

                // now, deserialize each
                activities = new List<Activity>();
                foreach (string json in jsonActivities)
                    activities.Add(JsonConvert.DeserializeObject<Activity>(json));

                return activities;
            }

            // look in database
            MySqlDataReader rdr = MySqlHelper.ExecuteReader(mysqlConnectionString,
                                                            Constants.FetchSql.Activities,
                                                            new MySqlParameter[] {
                                                                new MySqlParameter("@siteid", siteid),
                                                                new MySqlParameter("@sdate", date + "T00:00:00"),
                                                                new MySqlParameter("@edate", date + "T23:59:59")
                                                            });

            activities = new List<Activity>();
            while (rdr.Read())
            {
                Activity a;

                // check if the activity is already in the cache
                string idActivity = rdr["id"].ToString();
                string keyActivity = ActivityKey(idActivity);
                string json = cache.Get(keyActivity);
                if (json != null)
                {
                    a = JsonConvert.DeserializeObject<Activity>(json);
                }
                else
                {
                    a = new Activity();

                    PropertyInfo[] pis = typeof(Activity).GetProperties();
                    foreach (PropertyInfo pi in pis)
                    {
                        object value = rdr[pi.Name];
                        if (!DBNull.Value.Equals(value))
                        {
                            // treat booleans special
                            if (pi.PropertyType.Name == "Boolean")
                                pi.SetValue(a, rdr[pi.Name].ToString() != "0");
                            else
                                pi.SetValue(a, rdr[pi.Name]);
                        }
                    }

                    // add to activity cache
                    json = JsonConvert.SerializeObject(a);
                    cache.Store(ActivityKey(a.id), json);
                }

                // add to siteday cache
                activities.Add(a);
                cache.StoreInMap(siteDayKey, a.id, json);

            }

            // return
            return activities;

        }
        public void Cancel(Activity a)
        {
            MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString,
                                        Constants.DeleteSql.Activity,
                                        new MySqlParameter("@id", a.id));

            // Remove from caches
            cache.Remove(ActivityKey(a.id));
            cache.RemoveFromMap(SiteDayKey(a), a.id);

        }

        #endregion

        #region Resources
        public Resource GetResource(string id)
        {
            // check if in cache
            string key = ResourceKey(id);
            string json = cache.Get(key);
            if (json != null)
                return JsonConvert.DeserializeObject<Resource>(json);

            // read from database
            MySqlDataReader rdr = MySqlHelper.ExecuteReader(mysqlConnectionString,
                                                            Constants.FetchSql.Resource,
                                                            new MySqlParameter[] { new MySqlParameter("@id", id) });

            if (rdr.Read())
            {
                Resource r = new Resource();

                PropertyInfo[] pis = typeof(Resource).GetProperties();
                foreach (PropertyInfo pi in pis)
                {
                    object value = rdr[pi.Name];
                    if (!DBNull.Value.Equals(value))
                        pi.SetValue(r, rdr[pi.Name]);
                }

                // put in cache
                string s = JsonConvert.SerializeObject(r);
                cache.Store(key, s);

                return r;

            }
            return null;
        }

        public void StoreCenterResources(string id, ResourceTree resourceTree)
        {
            string key = CenterResourcesKey(id);

            // if it's already in the cache, it's already in the database - ignore it
            // TODO: do we store it anyway?
            if (IsCached(key))
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Ignoring duplicate: {0}", key));
                return;
            }

            // not cached. 
            string json = JsonConvert.SerializeObject(resourceTree);

            // cache
            cache.Store(key, json);

            // Write to database

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@id", id));
            parms.Add(new MySqlParameter("@json", json));

            // TODO: async?
            MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString, Constants.InsertSql.CenterResources, parms.ToArray());

            // Now, write out all of the individual resources
            WriteResourceTree(resourceTree);
        }

        private void WriteResourceTree(ResourceTree rt)
        {
            // write out the current node
            Resource r = new Resource();
            r.id = rt.id;
            r.name = rt.name;
            r.expanded = rt.expanded;
            r.type = rt.type;
            r.parentid = rt.parentid;

            // write to cache
            string json = JsonConvert.SerializeObject(r);
            cache.Store(ResourceKey(r.id), json);

            // now write to database
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("@id", r.id));
            parms.Add(new MySqlParameter("@name", r.name));
            parms.Add(new MySqlParameter("@expanded", r.expanded));
            parms.Add(new MySqlParameter("@type", r.type));
            parms.Add(new MySqlParameter("@parentid", r.parentid));

            // TODO: do this async?
            MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString, Constants.InsertSql.Resource, parms.ToArray());

            // and do the same with all the children
            if (rt.children != null)
                foreach (ResourceTree rtc in rt.children)
                    WriteResourceTree(rtc);

        }

        public ResourceTree GetCenterResources(string id)
        {
            string key = CenterResourcesKey(id);

            // check if in cache
            string json = cache.Get(key);
            if (json != null)
            {
                return JsonConvert.DeserializeObject<ResourceTree>(json);
            }

            MySqlDataReader rdr = MySqlHelper.ExecuteReader(mysqlConnectionString,
                                                            Constants.FetchSql.CenterResources,
                                                            new MySqlParameter[] { new MySqlParameter("@id", id) });

            if (rdr.Read())
            {
                json = (string)rdr["json"];
                ResourceTree rt = JsonConvert.DeserializeObject<ResourceTree>(json);

                // store in cache
                cache.Store(key, json);

                return rt;

            }
            return null;
        }

        public List<string> GetCenterResourceIds(string ecId)
        {
            Site site = GetSiteByEcId(ecId);
            return GetLmaCenterLaneResourceIds(site.Id);
        }

        public List<string> GetLmaCenterLaneResourceIds(string siteid)
        {
            // get the resource tree and flatten it
            ResourceTree rt = GetCenterResources(siteid);

            int resourceType = (int)ResourceType.BowlingLane;
            List<string> list = FlattenIdList(rt, resourceType);
            list.Sort();

            return list;
        }



        private List<String> FlattenIdList(ResourceTree rt, int resourceType,  List<string> list=null)
        {
            // if have no list, create it
            if (list == null)
                list = new List<string>();

            // add the current node to the list
            if (int.Parse(rt.type) == resourceType)
                list.Add(rt.id);

            // now add the children
            if (rt.children!=null && rt.children.Length>0)
                foreach (ResourceTree rtc in rt.children)
                    FlattenIdList(rtc, resourceType, list);

            return list;
        }


        #endregion

        #region Event
        public Event GetEvent(string id, bool withActivities)
        {
            string key = EventKey(id);

            // check if in cache
            string json = cache.Get(key);
            if (json != null)
            {
                return JsonConvert.DeserializeObject<Event>(json);

            }

            // read it from the database
            MySqlDataReader rdr = MySqlHelper.ExecuteReader(mysqlConnectionString,
                                                            Constants.FetchSql.Event,
                                                            new MySqlParameter[] { new MySqlParameter("@id", id) });
            if (rdr.Read())
            {
                Event ev = new Event();
                ev.id = rdr["id"] as string;
                ev.centerid = rdr["centerid"] as string;
                ev.activities = new List<Activity>();

                // store an empty event in the cache. This will get populated later
                json = JsonConvert.SerializeObject(ev);
                cache.Store(key, json);

                // get activities, only if requested to do so
                if (withActivities)
                {
                    ev.activities.AddRange(GetActivitiesForEvent(id));
                }

                return ev;
            }
            return null;

        }
        #endregion

        #region Sites

        public Site[] GetSites()
        {
            string key = "sites";

            // if it's in the cache, return it
            string json = cache.Get(key);
            if (json != null)
                return JsonConvert.DeserializeObject<Site[]>(json);

            // look it up
            MySqlDataReader rdr = MySqlHelper.ExecuteReader(mysqlConnectionString,
                                                            Constants.FetchSql.Sites);
            List<Site> sites = new List<Site>();
            while (rdr.Read())
            {
                Site s = new Site();
                s.Id = (string)rdr["id"];
                s.Name = (string)rdr["name"];
                s.SiteNumber = (int)rdr["siteNumber"];
                s.EntertainmentCenterId = (string)rdr["entertainmentCenterId"];
                sites.Add(s);

                // cache the site
                string sitekey = SiteKey(s.Id);
                cache.Store(sitekey, JsonConvert.SerializeObject(s));
            }

            // put in cache
            cache.Store(SitesKey(), JsonConvert.SerializeObject(sites));

            return sites.ToArray();
        }

        public Site GetSite(string siteid)
        {
            string key = string.Format("site-{0}", siteid);
            string json = cache.Get(key);
            if (json != null)
                return JsonConvert.DeserializeObject<Site>(json);

            // look in database
            MySqlDataReader rdr = MySqlHelper.ExecuteReader(mysqlConnectionString,
                                                            Constants.FetchSql.Site,
                                                            new MySqlParameter[] { new MySqlParameter("@id", siteid) });

            if (rdr.Read())
            {
                Site s = new Site();
                s.Id = (string)rdr["id"];
                s.Name = (string)rdr["name"];
                s.SiteNumber = (int)rdr["siteNumber"];
                s.EntertainmentCenterId = (string)rdr["entertainmentCenterId"];

                // cache the site
                cache.Store(key, JsonConvert.SerializeObject(s));

                return s;
            }
            return null;
        }

        public Site GetSiteByEcId(string ecId)
        {
            Site[] sites = GetSites();
            return sites.Single(s => s.EntertainmentCenterId == ecId);
        }

        public void StoreSites(Site[] sites)
        {
            foreach(Site s in sites)
            {
                MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString,
                                               Constants.InsertSql.Sites,
                                               new MySqlParameter[] { new MySqlParameter("@id", s.Id),
                                                                      new MySqlParameter("@entertainmentCenterId", s.EntertainmentCenterId),
                                                                      new MySqlParameter("@name", s.Name),
                                                                      new MySqlParameter("@siteNumber", s.SiteNumber)});
            }

            // put in cache
            cache.Store(SitesKey(), JsonConvert.SerializeObject(sites));
        }

        #endregion

    }

    static class LazyWriter
    {
        public static void Write(Object o)
        {
            Activity a = o as Activity;
            if (a == null)
                throw new Exception("Improper object passed to LazyWrite");
            System.Diagnostics.Debug.WriteLine("Writing activity " + a.id + " to the database");

            List<MySqlParameter> parms = new List<MySqlParameter>();
            PropertyInfo[] pis = typeof(Activity).GetProperties();
            foreach(PropertyInfo pi in pis)
            {
                parms.Add(new MySqlParameter("@" + pi.Name, pi.GetValue(a)));
            }
            MySqlHelper.ExecuteNonQuery(SqlStore.mysqlConnectionString, Constants.InsertSql.Activity, parms.ToArray());

        }
    }
}