﻿function GetGlobalContext() {
    console.log("Call to GCC");
    return {
        getUserId: function () { return 'C81F2774-5EF4-E511-80C2-005056AA8521'; },
        getUserName: function () { return 'Robert Roberts'; },
        regClientUrl: function () { return 'https://locahost:44350'}

    }
}
//code for loading all the sites
function loadSites() {
    var $select = $('#sites');
    //clear sites select
    $select.html('');
    //get json data from crm
    common._getSites(function (sites) {
        $select.append('<option value="-1"></option>');
        $.each(sites, function (key, val) {
            $select.append('<option  value="' + val.Id + '">' + val.Name + '</option>');
        });
        //get the siteid from URL
        var siteId = helper._getUrlVars()["site"];
        if (siteId != undefined && siteId != null) {
            $("#sites").val(siteId);
            
        }
        autocombobox._convertToCombobox();
    }
    );
};
//loading all the events
function loadEvents(siteId, start, end) {
    var isEventsFound = false;
    dp.events.list = [];
    dp.update();
    common._getEvents(siteId, start, end, function (crmEvents) {
        localEvents = crmEvents;
        isEventsFound = true;
        $.each(crmEvents, function (key, val) {
            // alert(val.start);
            var e = new DayPilot.Event({
                start: new DayPilot.Date(val.start),
                end: new DayPilot.Date(val.end),
                id: val.id,
                resource: val.resource,
                text: val.text,
                backColor: val.backColor,
                repname: val.repname,
                partyname: val.partyname,
                guestcount: val.guestcount,
                onholddate: val.onholddate,
                totalrevenue: val.totalrevenue,
                oppoid: val.oppoid,
                resizeDisabled: true,
                isretailhold: val.isretailhold,
                iseventhold: val.iseventhold,
                createdbyname: val.createdbyname
            });
            dp.events.add(e);
        });
        //dp.message("Events Loaded!!");
        if (isEventsFound) {
            dp.message("Events Loaded!!", 1000, "#ffff", "#dc143c");
        }
    });

}
//loading all the resources
function loadResources(siteId) {
    barriers = [];
    timeSensitiveResources = [];
    common._getResources(siteId, function (resources) {
        localResources = [];
        resourceColors=[];
        getListResources(resources);
        var i = 0; j = 0;
        $.each(resources, function (key, val) {
            if (val.type == "Floor" || val.type == "PhysicalBarrier" || val.type == "NotSpecified") {
                barriers.push(val.id);
            }
        });
        resourceColors = getColorCodeResources(resources, i, resourceColors);
        timeSensitiveResources = getTimeSensitiveResources(resources, j, timeSensitiveResources);
        dp.resources = resources;
        dp.update();
    });
}

function getTimeSensitiveResources(resources, j, timeSenResource) {
    $.each(resources, function (key, val) {
        if (val.type == "BowlingLane" || val.type == "Room")//BowlingLane and rooms are time sensitive resources, any changes to time create contract revision
        {
            timeSenResource.push(val.id);
        }
        j++;
        if (val.children != null && val.children != undefined && val.children != "undefined" && val.children.length > 0) {
            getTimeSensitiveResources(val.children, j, timeSenResource);
            j = j + val.children.length;
        }
    });
    return timeSenResource;
}
function getColorCodeResources(resources, i, resourceColors)
{
    $.each(resources, function (key, val) {
        if (i % 2 == 0)//even rows use light gray color #deeff5
        {
            resourceColors.push(val.id);
        }
        i++;
        if (val.children != null && val.children != undefined && val.children != "undefined" && val.children.length > 0) {
            getColorCodeResources(val.children, i, resourceColors);
            i = i + val.children.length;
        }
    });
    return resourceColors;
}
//cancelling activity
function cancelActivity(event, e) {
    common._cancelActivity(event, function () {
        dp.events.remove(e);
        dp.message("Event Cancelled Successfully!!", 5000, "#fff", "#dc143c");
    });
}
//update activity
function updateActivity(event, e) {
    common._updateActivity(event, function () {
        ////update local list of events
        $.each(localEvents, function (key, val) {
            if (val.resource == e.resource()
                && val.start == e.start().value &&
                val.end == e.end().value) {
                val.resource = event.resource;
                val.start = event.start;
                val.end = event.end;
                return;
            }
        });
        e.resource(event.resource);
        e.start(event.start);
        e.end(event.end);
        dp.events.update(e);

        dp.message("Event Updated!!", 5000, "#fff", "#dc143c");
    });

}
//update list of activities in CRM
function updateActivities(events, isTimeChanged) {
    if (events[0].oppoid == "" || events[0].oppoid == undefined || events[0].oppoid == null)
    {
        //retail event is being updated
        dp.message("Updating event(s)!", 5000, "#fff", "#dc143c");
    }
    else if (isTimeChanged)
    {
        dp.message("Updating event(s) and revising contract!", 5000, "#fff", "#dc143c");
    }
    else
    {
        dp.message("Updating event(s)!", 5000, "#fff", "#dc143c");
    }
    common._updateActivities(events, function () {
        //retail event is being updated
        if (events[0].oppoid == "" || events[0].oppoid == undefined || events[0].oppoid == null) {
            //refresh data from CRM, executing siteonclick to refresh data
            //loadSiteData();
            refreshEvents();
            dp.message("Event(s) Updated!", 5000, "#fff", "#dc143c");
        }
        //if time is changed, update opportunity & contract to reflect changes
        else if (isTimeChanged) {
            //check if multiple activities are processed/
            //call method to change event start & end and also revise contract
            common._updateEvent(events[0], function () {
                dp.multiselect.clear();
                //refresh data from CRM, executing siteonclick to refresh data
                refreshEvents();
                dp.message("Event(s) Updated & Contract Revised!!", 5000, "#fff", "#dc143c");
            });
        }
        else
        {
            dp.multiselect.clear();
            //refresh data from CRM, executing siteonclick to refresh data
            refreshEvents();
            dp.message("Event Updated!!", 5000, "#fff", "#dc143c");
        }
        
    });
}
//create activity
function createActivity(event) {
    if (event.isretailhold) {
        dp.message("Creating Retail Hold!!", 5000, "#fff", "#dc143c");
    }
    else {
        dp.message("Creating Event Hold!!", 5000, "#fff", "#dc143c");
    }
    common._createActivity(event, function (val) {
        refreshEvents();
        dp.message("Event Created!!", 5000, "#fff", "#dc143c");
    });
}
//get list of resources
function getListResources(resources) {
    $.each(resources, function (key, val) {
        var rs = new entities.resource(val.name, val.id, val.expanded, val.children, val.type);
        localResources.push(rs);
        if (val.expanded) {
            getListResources(val.children);
        }
    });
}
//get resource by id
function resourceById(rid) {
    var rs = {};
    $.each(localResources, function (key, val) {
        if (val.id == rid) {
            rs = val;
            return;
        }
    });
    return rs;
}
//check if resource is child
function isChild(children,resource)
{
    var isChild = false;
    $.each(children, function (key, val) {
        if (val.id == resource) {
            isChild = true;
            return;
        }
    });
    return isChild;
}
//check if event is buffer activity
function isBuffer(eventId, allEvents) {
    var isBuffer = false;
    $.each(allEvents, function (key, val) {
        if (val.id == eventId && val.isbufferactivity) {
            isBuffer = true;
            return isBuffer;
        }
    });
    return isBuffer;
}
//get resource by id
function eventById(eid) {
    var ev = {};
    $.each(localEvents, function (key, val) {
        if (val.id == eid) {
            ev = val;
            return;
        }
    });
    return ev;
}
//code for getting activites based on start & end date and entertainment center selected.
function loadSiteData() {
    var siteId = getCurrentSiteId();
    // alert(siteId);
    if (siteId != -1 && siteId != undefined && siteId != null) {
        loadResources(siteId);
        refreshEvents(siteId);
        //move scroll to time passed
        var st = helper._getUrlVars()["st"];
        if (st != undefined && st != "undefined" && st != "") {
            dp.scrollTo(dp.startDate.addHours(st));
        } else {
            dp.scrollTo(dp.startDate.addHours(12));
        }
        dp.update();    
    }

}
//getting events from crm
function refreshEvents() {
    var siteId = getCurrentSiteId();
    if (siteId == undefined || siteId == '')
    {
        return;
    }
    //var siteId = $('#sites').find(":selected").val();
    var startDate = new Date(nav.selectionDay.addHours(12)); // new Date(nav.selectionDay.substring(0,nav.selectionDay.indexOf('T')));
    var enddate = helper._addDays(startDate, 3);
    var start = (startDate.getMonth() + 1) + '-' + startDate.getDate() + '-' + startDate.getFullYear();
    var end = (enddate.getMonth() + 1) + '-' + enddate.getDate() + '-' + enddate.getFullYear();
    loadEvents(siteId, start, end);
}
function getCurrentSiteId() {
    var siteId = $('#sites').find(":selected").val();
    if(siteId == '' || siteId == undefined)
    {
        siteId = helper._getUrlVars()["site"];
    }
    return siteId;
}
//setting dates from query string parameters
function setDates() {
    var startDate = helper._getUrlVars()["sd"];
    var siteId = helper._getUrlVars()["site"];
    var config = helper._getUrlVars()["config"];
    if (config == "true")
    {
        dp.height = 220;
        dp.update();
    }
    if (siteId != undefined && siteId != null) {
      //  $("#sites").val(siteId);
        var dpDate = new DayPilot.Date(startDate);
        if (dpDate.getDatePart().value == nav.selectionDay.getDatePart().value) {
            //as site is not selected initially we will load the data after setting data
            loadSiteData();
        }
        else {           
            nav.select(dpDate);
        }
        //else {
          //  nav.select(dpDate);
        //}
    }
};