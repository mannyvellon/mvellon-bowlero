﻿if (typeof (restapi) == "undefined")
{ restapi = { __namespace: true }; }
restapi = {
    _getCrmServerUrl: function () {
        return "https://localhost/44353";
    },
    _errorfn: function (request, message, error)
    {
        if (request.responseText!= undefined && request.responseText!="") {
            // only parse the response if you know it is JSON
            var error = $.parseJSON(request.responseText);
            alert(error.Message);
        }
        else if (request.responseText == "" || request.responseText == undefined) {
            //this would be not found exception from server as per request from customer not displaying any error.
        }
        else {
            alert('An error occurred while processing your request.');
        }

    },
    _restServiceURL: function () {
        ///<summary>
        /// service url
        ///</summary>
        ///<returns>rest service url</returns>
        //local variable from html file

        //TODO
       //return "http://localhost:61985/";
       //return "http://dev-gemsns01.na.amfbowl.net/";
            return "https://localhost:44353/";

    },
    _get: function (url, successfn) {
          $.ajax({
              dataType: "json",
              async: true,
            url: this._restServiceURL() + url,//'activities/GetLActivities',
            success: successfn,
            error: this._errorfn
          });
    },

    _post: function (url, data, successfn) {
        $.ajax({
            type: "POST",
            dataType: "json",
            async: true,
            contentType : "application/json",
            url: this._restServiceURL() + url,//'activities/lanegrid/updateserviceactivity',
            data: data,
            success: successfn,
            error: this._errorfn
        });
    }
}
