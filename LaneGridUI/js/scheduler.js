﻿if (typeof (scheduler) == "undefined")
{ scheduler = { __namespace: true }; }
scheduler = {
    _init: function () {

        nav = new DayPilot.Navigator("nav");
        nav.theme = "navigator_blue";
        nav.showMonths = 2;
        nav.selectMode = "day";
        nav.onTimeRangeSelected = function (args) {
            dp.startDate = args.start;
            //alert(args.days);
            dp.days = args.days + 2; //displaying current day + 2 days
            loadSiteData();
            dp.update();
        };
        nav.init();
        // view
        //  dp.startDate = new DayPilot.Date("2013-03-24");  // or just dp.startDate = "2013-03-25";
        // view
        dp = new DayPilot.Scheduler("dp");
        dp.heightSpec = "Max";
        dp.height = 600;
        // behavior and appearance
        //dp.cssClassPrefix = "scheduler_transparent";
        dp.theme = "scheduler_blue";


        //     dp.resources = [];
        dp.startDate = nav.selectionStart;  // or just dp.startDate = "2013-03-25";
        dp.cellGroupBy = "Month";
        dp.cellWidth = 25;
        dp.days = 3;
        dp.scale = "CellDuration";
        dp.cellDuration = 15;
        // dp.days = 7;
        //dp.cellDuration = 1440; // one day

        dp.timeHeaders = [
             { groupBy: "Day" },
            { groupBy: "Hour" },
            { groupBy: "Cell" }
        ];
        //business hours
        dp.businessBeginsHour = 0;
        dp.businessEndsHour = 24;
        dp.showNonBusiness = true;
        dp.eventDoubleClickHandling = true;
        //context menu for Cancel
        dp.contextMenu = new DayPilot.Menu({
            items: [
                {
                    text: "Cancel",

                    onclick: function () {

                        var e = this.source;
                        var ev = new entities.event(e.id());
                        if (common._isRetailHold(e.id(), localEvents) || common._isEventHold(e.id(), localEvents)) {

                            cancelActivity(ev, e);
                            $.each(localEvents, function (key, val) {
                                if (val.id == e.id()) {
                                    val.isDeleted = true;
                                    return;
                                }
                            });
                        }
                        else {
                            alert('Only retail activities can be cancelled!')
                        }
                    }
                },
            ],
            cssClassPrefix: "menu_default"
        });
        //to enable tree view for resources
        dp.treeEnabled = true;
        //dp.eventClickHandling = "Select";
        dp.allowMultiMove = true;
        dp.multiMoveVerticalMode = "All";
        //dps.allowEventOverlap = false;
        // bubble, sync loading
        // see also DayPilot.Event.data.staticBubbleHTML property
        dp.bubble = new DayPilot.Bubble();
        dp.eventHoverHandling = "Bubble";

        dp.init();

        dp.onBeforeEventRender = function (args) {
            var html = "";
            if (args.e.isretailhold || args.e.iseventhold) {
                html = "<div>Created By: " + args.e.createdbyname + "</div>"
                + "<div>On Hold Date: " + args.e.onholddate + "</div>";
            }
            else if (args.e.onholddate != "") {
                html = "<div>Event Name:<b>" + args.e.text + "</b></div>"
                    + "<div>Party Name: " + args.e.partyname + "</div>"
                  + "<div>Rep Name: " + args.e.repname + "</div>"
                  + "<div>Guest Count: " + args.e.guestcount + "</div>"
               + "<div>On Hold Date: " + args.e.onholddate + "</div>"
               + "<div>Total Revenue: " + args.e.totalrevenue + "</div>";
            }
            else {
                html = "<div>Event Name:<b>" + args.e.text + "</b></div>"
                                     + "<div>Party Name: " + args.e.partyname + "</div>"
                                   + "<div>Rep Name: " + args.e.repname + "</div>"
                                   + "<div>Guest Count: " + args.e.guestcount + "</div>"
                                + "<div>Total Revenue: " + args.e.totalrevenue + "</div>";
            }
            args.e.bubbleHtml = html;
        };
        //disable resize of event
        dp.onEventResized = function (args) {
            // args.preventDefault();
            return;
        };
        $(document).keyup(function (e) {
            // enter
            if (e.keyCode === 27) dp.multiselect.clear();   // esc
        });
        // event creating
        dp.onTimeRangeSelected = function (args) {
            //checking it the ctrl is pressed and skip retail if that is pressed
            if (args.ctrl) {
                args.preventDefault();
                return;
            }
            var ans = confirm('Do you want to create event hold?');
            var name = ""; var isEventHold = false; isRetailHold = false;
            dp.multiselect.clear();
            if (ans) {
                name = prompt("Event Hold Description:", "Event Hold");
                isEventHold = true;
            }
            else {
                name = prompt("Retail Hold Description:", "Retail Hold");
                isRetailHold = true;
            }

            var isBooked = false;
            //check if resource is booked with other events
            $.each(localEvents, function (key, val) {
                if (val.resource == args.resource  //check if both event are at same resource
                   && !val.isDeleted && ((val.start <= args.start && val.end > args.start) //existing event start date is less than current and end is greater than start
                  || (val.start > args.start && val.end <= args.end) //existing event start date is equal to current and end is greater than start                       
                    || (val.start < args.end && val.end > args.end) //existing event start date is equal to current and end is greater than start                          
                    )) {
                    isBooked = true;
                    return;
                }
            });
            if (isBooked) {
                alert('Resource(s) is already booked!');
                args.preventDefault();
                dp.clearSelection();
                return;
            }
            //check if 
            dp.clearSelection();
            if (!name) return;
            var e = new DayPilot.Event({
                start: args.start,
                end: args.end,
                id: DayPilot.guid(),
                resource: args.resource,
                text: name,
                backColor: "Grey",
                repname: "",
                partyname: "",
                guestcount: "",
                onholddate: "",
                totalrevenue: "",
                oppoid: "",
                isretailhold: true,
                resizeDisabled: true
            });

            //TODO - CRM
            var userid = GetGlobalContext().getUserId();
            var currentUserName = GetGlobalContext().getUserName();
            //local
            //TODO
            // var userid = "C81F2774-5EF4-E511-80C2-005056AA8521";
            //var currentUserName = "Amol";
            var siteId = $('#sites').find(":selected").val();
            //var currentUserName = "amol";
            var ev = new entities.event(e.id(), args.resource, args.end.value, args.start.value, userid, name, siteId, "Grey", "", "", "", "", "", isRetailHold, isEventHold, "", currentUserName);
            createActivity(ev, e);
        };
        // event moving
        //dp.eventMoveHandling = "JavaScript";
        dp.onEventMove = function (args) {

            var e = args.e;
            if ($.inArray(args.newResource, barriers) > -1) {
                alert('Cannot book resource!');
                args.preventDefault();
                return;
            }
            //check if it is league activity
            if (common._isLeague(e.id(), localEvents)) {
                alert('Cannot move League activities!');
                args.preventDefault();
                return;
            }
            //get resource object type 
            if (resourceById(e.resource()).type != resourceById(args.newResource).type) {
                alert('Cannot change to different type of resource!');
                args.preventDefault();
                return;
            }
        };
        dp.onEventMoved = function (args) {

            //update events send to crm
            var evs = [];
            var isTimeChanged = false;
            var isBooked = false; var isOutOppoBound = false;
            var isTimeSensitive = false;
            var isDiffResource = false;
            //validate if all the resources are avalible
            args.multimove.map(function (item) {
                var e = item.event;
                var currentEvent = eventById(e.id());
                //check if resource is booked with other events
                $.each(localEvents, function (key, val) {
                    if (val.id == e.id() && resourceById(e.resource()).type != resourceById(val.resource).type) {
                        console.log("local event " + val.id + " current event " + e.id() + " resource type local " + resourceById(val.resource).type
                            + " resource type current " + resourceById(e.resource()).type);
                        isDiffResource = true;
                        return;
                    }
                    if (val.resource == e.resource()  //check if both event are at same resource
                      && !val.isDeleted && val.id != e.id()          //check if both events are not same
                        && (args.multimove.length == 1 || currentEvent.oppoid != val.oppoid) //check if it is related resource movement and checking overbooking among children
                        && ((e.start().value < val.start && e.end().value > val.start && !val.isbufferactivity) //existing event start date is less than current and end is greater than start
                      || (e.start().value >= val.start && e.start().value < val.end && !val.isbufferactivity)  //existing event start date is equal to current and end is greater than start                       
                        )) {
                        isBooked = true;
                        return;
                    }
                });

                // args.e.resource(args.newResource);
                //TODO CRM
                var userid = GetGlobalContext().getUserId();
                var oppId = currentEvent.oppoid;
                if (currentEvent.start != e.start() || currentEvent.end != e.end()) {
                    isTimeChanged = true;
                }
                //check if the time has changes and the resource is one of the time sensitive resource
                if (isTimeChanged && !isBuffer(e.id(), localEvents) && $.inArray(e.resource(), timeSensitiveResources) > -1) {
                    isTimeSensitive = true;
                }
                //check event movement is outside event start & end bound
                var oppoStart = new DayPilot.Date(currentEvent.oppoStart);
                var oppoEnd = new DayPilot.Date(currentEvent.oppoEnd);
                if ((e.start() < oppoStart || e.end() > oppoEnd) && !isBuffer(e.id(), localEvents)) {
                    isOutOppoBound = true;
                }

                //local
                // var userid = "C81F2774-5EF4-E511-80C2-005056AA8521";

                if (args.multimove != undefined && args.multimove != null && args.multimove.length > 1) {
                    ev = new entities.event(e.id(), e.resource(), e.end(), e.start(), userid, "", "", "", oppId);
                }
                else {
                    //for single move, don't allow events to change the timings
                    ev = new entities.event(e.id(), args.newResource, args.newEnd.value, args.newStart.value, userid, "", "", "", oppId);
                }
                evs.push(ev);
            });
            if (isDiffResource) {
                alert('Cannot change to different type of resource!');
                args.preventDefault();
                refreshEvents();
                return;
            }
            if (isBooked) {
                alert('Resource(s) is already booked!');
                args.preventDefault();
                refreshEvents();
                return;
            }
            if (isTimeSensitive) {
                var ans = confirm('Changing time would revise the contract, do you still want to continue?');
                if (ans) {
                    updateActivities(evs, isTimeSensitive);
                }
                else {
                    args.preventDefault();
                    refreshEvents();
                    return;
                }
            }
            else if (isOutOppoBound) {
                var ans = confirm('Resource(s) are moved outside contract start/end time and it would revise the contract, do you still want to continue?');
                if (ans) {
                    updateActivities(evs, isTimeSensitive);
                }
                else {
                    args.preventDefault();
                    refreshEvents();
                    return;
                }
            }
            else {
                updateActivities(evs);
            }
        };

        dp.onBeforeCellRender = function (args) {
            if ($.inArray(args.cell.resource, barriers) > -1) {
                args.cell.backColor = "black";
            }
            else {
                //providing alternate color for lane grid
                if ($.inArray(args.cell.resource, resourceColors) > -1) {
                    args.cell.backColor = "#e7f5fe";
                }
            }

        };

        dp.onEventDoubleClick = function (args) {
            //  alert(restapi._restServiceURL());
            // alert("clicked: " + args.e.id());
            var url = common._getOppoUrl(args.e.id(), localEvents);
            if (url != undefined) {
                window.open(url);
            }
        };

        dp.onEventClick = function (args) {
            //check if selected activity is League event
            //don't allow user to select it
            if (common._isLeague(args.e.id(), localEvents)) {
                //
                alert('Cannot select League activities.');
                args.preventDefault();
                return;
            }
            if (args.ctrl) {
                dp.multiselect.add(args.e);   // add to selection 
                args.preventDefault();        // cancel the default action
            }
            else {
                var ans = confirm('Do you want to select related resources?');
                dp.multiselect.clear();
                if (ans) {
                    //dp.multiselect.clear();  
                    var relatedEvents = common._getRelated(args.e.id(), localEvents);
                    var resId = args.e.resource();
                    var selectedResource = resourceById(resId);
                    $.each(relatedEvents, function (val, e) {
                        var childResource = resourceById(e.resource);
                        if ((selectedResource.type == childResource.type) ||
                            (selectedResource.children != null && selectedResource.children != undefined && selectedResource.children.length > 0 && isChild(selectedResource.children, childResource.id))) {
                            //need to add logic to select related events
                            var e1 = new DayPilot.Event({
                                id: e.id,
                                start: e.start,
                                end: e.end,
                                resource: e.resource
                            });
                            dp.multiselect.add(e1);
                        }
                        //write to code to update CRM
                    });
                }
                else {
                    dp.multiselect.add(args.e);
                }
            };
        }

    }
}