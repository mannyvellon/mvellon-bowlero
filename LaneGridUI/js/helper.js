﻿if (typeof (helper) == "undefined")
{ helper = { __namespace: true }; }
helper = {
    _getUrlVars: function () {
        var vars = [], hash;
        var dUrl = decodeURIComponent(window.location.href);
        var hashes = dUrl.slice(dUrl.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    _getUrlVar: function (name) {
        return $.getUrlVars()[name];
    },
    //function to add days to a given date. 
    _addDays : function(startDate, numberOfDays) {
    var returnDate = new Date(
                        startDate.getFullYear(),
                        startDate.getMonth(),
                        startDate.getDate() + numberOfDays
                        );
    return returnDate;
    // return year + '-' + month + '-' + (date + numberOfDays);
    //return returnDate;
}
       
}

